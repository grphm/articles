@extends("core_system_views::layouts.$current_layout")
@section('title', trans('core_system_lang::dashboard.control_panel'))
@section('breadcrumb')
    <ol class="breadcrumb">
        <li>
            <a href="{{ route('dashboard') }}">
                <i class="zmdi zmdi-view-dashboard"></i> @lang('core_system_lang::system.dashboard')
            </a>
        </li>
        <li class="c-gray">
            <i class="{{ config('solutions_articles::menu.icon') }}"></i> {!! array_translate(config('solutions_articles::menu.title')) !!}
        </li>
        <li>
            <a href="{{ route('core.galleries.templates.index') }}">
                <i class="{{ config('solutions_articles::menu.menu_child.templates.icon') }}"></i> {!! array_translate(config('solutions_articles::menu.menu_child.templates.title')) !!}
            </a>
        </li>
        <li class="active">
            <i class="zmdi zmdi-edit"></i> @lang('solutions_articles_lang::templates.replace.breadcrumb')
        </li>
    </ol>
@stop
@section('content')
    <div class="block-header">
        <h2><i class="zmdi zmdi-edit"></i> @lang('solutions_articles_lang::templates.replace.title')</h2>
    </div>
    <div class="card">
        <div class="card-body card-padding">
            {!! Form::model($template, ['route' => ['solutions.articles.templates.update', $template->id], 'class' => 'form-validate', 'id' => 'edit-article-rubric-template-form', 'method' => 'PUT']) !!}
            <div class="row">
                <div class="col-sm-4">
                    <div class="form-group fg-float">
                        <div class="fg-line">
                            {!! Form::text('title', NULL, ['class'=>'input-sm form-control fg-input']) !!}
                            <label class="fg-label">@lang('solutions_articles_lang::templates.replace.form.title')</label>
                        </div>
                        <small class="help-description">@lang('solutions_articles_lang::templates.replace.form.title_help_description')</small>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-12">
                    <div class="form-group">
                        <p class="c-gray m-b-20">@lang('solutions_articles_lang::templates.replace.form.content')</p>
                        <pre id="template_content">{{ $template_content }}</pre>
                        {!! Form::textarea('content', NULL, ['class' => 'hidden', 'data-autosize-on' => 'true']) !!}
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-4">
                    <button type="submit" autocomplete="off" class="btn btn-primary btn-sm m-t-10 waves-effect">
                        <i class="fa fa-save"></i>
                        <span class="btn-text">@lang('solutions_articles_lang::templates.replace.form.submit')</span>
                    </button>
                </div>
            </div>
            {!! Form::close() !!}
        </div>
    </div>
@stop
@section('scripts_after')
    {!! Html::script('core/ace/ace.js') !!}
    <script>
        var editor = ace.edit("template_content");
        editor.setTheme("ace/theme/monokai");
        editor.getSession().setMode("ace/mode/javascript");
        editor.getSession().setUseSoftTabs(true);
        document.getElementById('template_content').style.fontSize = '14px';
        editor.getSession().setUseWrapMode(true);
        editor.setShowPrintMargin(false);
        editor.setOptions({
            maxLines: Infinity
        });
        $("#edit-article-rubric-template-form button[type='submit']").click(function () {
            $("#edit-article-rubric-template-form textarea[name='content']").val(editor.getValue());
        });

        var form = $("#edit-article-rubric-template-form");
        BASIC.currentForm = form;
        BASIC.validateOptions.rules = {title: {required: true}};
        BASIC.validateOptions.messages = VALIDATION_MESSAGES.defaulRules;
        $(BASIC.currentForm).validate(BASIC.validateOptions);
    </script>
@stop