@extends("core_system_views::layouts.$current_layout")
@section('title', trans('core_system_lang::dashboard.control_panel'))
@section('breadcrumb')
    <ol class="breadcrumb">
        <li>
            <a href="{{ route('dashboard') }}">
                <i class="zmdi zmdi-view-dashboard"></i> @lang('core_system_lang::system.dashboard')
            </a>
        </li>
        <li class="c-gray">
            <i class="{{ config('solutions_articles::menu.icon') }}"></i> {!! array_translate(config('solutions_articles::menu.title')) !!}
        </li>
        <li class="active">
            <i class="{{ config('solutions_articles::menu.menu_child.templates.icon') }}"></i> {!! array_translate(config('solutions_articles::menu.menu_child.templates.title')) !!}
        </li>
    </ol>
@stop
@section('content')
    <div class="block-header">
        <h2>
            <i class="{{ config('solutions_articles::menu.menu_child.templates.icon') }}"></i>
            {!! array_translate(config('solutions_articles::menu.menu_child.templates.title')) !!}
        </h2>
    </div>
    @BtnAdd('solutions.articles.templates.create')
    <div class="card">
        <div class="list-group lg-odd-black">
            <div class="action-header clearfix">
                <ul class="actions">
                    <li class="dropdown">
                        <a href="" data-toggle="dropdown" aria-expanded="true">
                            <i class="zmdi zmdi-filter-list"></i>
                        </a>
                        <ul class="dropdown-menu dropdown-menu-right">
                            <li>
                                <a href="{{ route('solutions.articles.templates.index') }}">@lang('solutions_articles_lang::templates.all_types')</a>
                            </li>
                            <li class="divider"></li>
                            @foreach(config('solutions_articles::config.template_types') as $menu_type_slug => $menu_type)
                                <li>
                                    <a href="{{ route('solutions.articles.templates.index') . '?type=' . $menu_type_slug }}">
                                        {{ array_translate($menu_type) }}
                                    </a>
                                </li>
                            @endforeach
                        </ul>
                    </li>
                </ul>
            </div>
            <div class="card-body card-padding m-h-250">
                @if(count($templates))
                    <div class="row">
                        @foreach($templates as $type_slug => $type_templates)
                            <div class="col-sm-{!! 12 / count($templates) !!} m-b-25">
                                <p class="f-500 c-black m-b-0">{{ $type_templates['title'] }}</p>
                                <small>@lang('solutions_articles_lang::templates.root_directory')</small>
                                @forelse($type_templates['files'] as $file)
                                    <div class="js-item-container list-group-item media p-15">
                                        <div class="pull-right">
                                            <div class="lv-actions actions dropdown">
                                                <a aria-expanded="true" data-toggle="dropdown" href="">
                                                    <i class="zmdi zmdi-more-vert"></i>
                                                </a>
                                                <ul class="dropdown-menu dropdown-menu-right">
                                                    <li>
                                                        <a href="{{ route('solutions.articles.templates.edit', $file->id) }}">
                                                            @lang('solutions_articles_lang::templates.edit')
                                                        </a>
                                                    </li>
                                                    @if($file->required)
                                                    @else
                                                        <li class="divider"></li>
                                                        <li>
                                                            <a class="c-red js-item-remove" href="">
                                                                @lang('solutions_articles_lang::templates.delete.submit')
                                                            </a>
                                                            {!! Form::open(['route' => ['solutions.articles.templates.destroy', $file->id], 'method' => 'DELETE', 'class' => 'hidden']) !!}
                                                            <button type="submit"
                                                                    data-question="@lang('solutions_articles_lang::templates.delete.question') &laquo;{{ $file->title }}&raquo;?"
                                                                    data-confirmbuttontext="@lang('solutions_articles_lang::templates.delete.confirmbuttontext')"
                                                                    data-cancelbuttontext="@lang('solutions_articles_lang::templates.delete.cancelbuttontext')">
                                                            </button>
                                                        {!! Form::close() !!}
                                                    @endif
                                                </ul>
                                            </div>
                                        </div>
                                        <div class="media-body">
                                            <div class="lgi-heading">{{ $file->title }}</div>
                                            <small class="lgi-text"><strong>{{ $file->path }}</strong></small>
                                        </div>
                                    </div>
                                @empty
                                    <h2 class="f-16 c-gray">@lang('solutions_articles_lang::templates.empty')</h2>
                                @endforelse
                            </div>
                        @endforeach
                    </div>
                    <div class="clearfix"></div>
                @else
                    <h2 class="f-16 c-gray">@lang('solutions_articles_lang::templates.empty')</h2>
                @endif
            </div>
        </div>
    </div>
@stop