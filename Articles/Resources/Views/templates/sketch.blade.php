@extends('home_views::layout')
@metaTitle()
@metaDescription()
@OpenGraph()
@section('content')
    <h1>@PageTitle()</h1>
    <div>
        <p>{{ $page->announce }}</p>
    </div>
    @if(!empty($page->tags))
        <div>
            <h3>Tags</h3>
            <ul>
                @foreach(\PublicArticle::getRubricTags($page->id) as $tag_title => $tag_count)
                    <li>{!! $tag_title !!}</li>
                @endforeach
            </ul>
        </div>
    @endif
    @if($articles->count())
        <div>
            <h3>Articles</h3>
            <ul>
                @foreach($articles as $article)
                    <a href="{!! route('public.articles.show', $article->PageUrl) !!}">{{ $article->title }}</a>
                    <p>
                        {{ $article->announce }}
                    </p>
                @endforeach
            </ul>
        </div>
    @endif
@stop