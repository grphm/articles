@extends("core_system_views::layouts.$current_layout")
@section('title', trans('core_system_lang::dashboard.control_panel'))
@section('breadcrumb')
    <ol class="breadcrumb">
        <li>
            <a href="{{ route('dashboard') }}">
                <i class="zmdi zmdi-view-dashboard"></i> @lang('core_system_lang::system.dashboard')
            </a>
        </li>
        <li class="c-gray">
            <i class="{{ config('solutions_articles::menu.icon') }}"></i> {!! array_translate(config('solutions_articles::menu.title')) !!}
        </li>
        <li>
            <a href="{{ route('solutions.articles.rubrics.index') }}">
                <i class="{{ config('solutions_articles::menu.menu_child.rubrics.icon') }}"></i> {!! array_translate(config('solutions_articles::menu.menu_child.rubrics.title')) !!}
            </a>
        </li>
        <li class="active">
            <i class="zmdi zmdi-edit"></i> @lang('solutions_articles_lang::rubrics.replace.breadcrumb')
        </li>
    </ol>
@stop
@section('content')
    <div class="block-header">
        <h2>
            <i class="zmdi zmdi-edit"></i> @lang('solutions_articles_lang::rubrics.replace.title')
        </h2>
    </div>
    <div class="card">
        <div class="card-body card-padding">
            <div class="row">
                {!! Form::model($rubric, ['route' => ['solutions.articles.rubrics.update', $rubric->id], 'class' => 'form-validate', 'id' => 'edit-solutions-article-rubric-form', 'method' => 'PUT', 'files' => TRUE]) !!}
                <div class="col-sm-10">
                    <div class="form-group fg-float">
                        <div class="fg-line">
                            {!! Form::text('title', NULL, ['class'=>'input-sm form-control fg-input']) !!}
                            <label class="fg-label">@lang('solutions_articles_lang::rubrics.replace.form.title')</label>
                        </div>
                    </div>
                    <div class="form-group fg-float">
                        <div class="fg-line">
                            {!! Form::textarea('announce', NULL, ['class' => 'form-control auto-size fg-input', 'data-autosize-on' => 'true', 'rows' => 1]) !!}
                            <label class="fg-label">@lang('solutions_articles_lang::rubrics.replace.form.announce')</label>
                        </div>
                    </div>
                    <div class="form-group">
                        <p class="c-gray m-b-20">@lang('solutions_articles_lang::rubrics.replace.form.content')</p>
                        {!! Form::textarea('content', NULL, ['id' => 'rubric_content']) !!}
                    </div>
                    <div class="row">
                        <div class="col-sm-4">
                            <p class="c-gray m-b-5">@lang('solutions_articles_lang::rubrics.replace.form.main_image')</p>
                            <small class="help-description">@lang('solutions_articles_lang::rubrics.replace.form.main_image_help_description')</small>
							<div class="clearfix"></div>
                            <div class="fileinput fileinput-new m-t-5" data-provides="fileinput">
                                <div class="fileinput-preview thumbnail" style="line-height: 150px;" data-trigger="fileinput">
                                    @if($rubric->main_image)
                                        <img src="{!! asset('uploads' . $rubric->main_image)!!}">
                                    @endif
                                </div>
                                <div>
                                    <div class="btn btn-info btn-file">
                                        <span class="fileinput-new m-t-5">@lang('solutions_articles_lang::rubrics.replace.form.image_select')</span>
                                        <span class="fileinput-exists">@lang('solutions_articles_lang::rubrics.replace.form.image_change')</span>
                                        {!! Form::file('main_image') !!}
                                    </div>
                                    <a href="#" class="btn btn-danger fileinput-exists"
                                       data-dismiss="fileinput">@lang('solutions_articles_lang::rubrics.replace.form.image_delete')</a>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <p class="c-gray m-b-5">@lang('solutions_articles_lang::rubrics.replace.form.announce_image')</p>
                            <small class="help-description">@lang('solutions_articles_lang::rubrics.replace.form.announce_image_help_description')</small>
							<div class="clearfix"></div>
                            <div class="fileinput fileinput-new" data-provides="fileinput">
                                <div class="fileinput-preview thumbnail" style="line-height: 150px;" data-trigger="fileinput">
                                    @if($rubric->announce_image)
                                        <img src="{!! asset('uploads' . $rubric->announce_image)!!}">
                                    @endif
                                </div>
                                <div>
                                    <div class="btn btn-info btn-file">
                                        <span class="fileinput-new">@lang('solutions_articles_lang::rubrics.replace.form.image_select')</span>
                                        <span class="fileinput-exists">@lang('solutions_articles_lang::rubrics.replace.form.image_change')</span>
                                        {!! Form::file('announce_image') !!}
                                    </div>
                                    <a href="#" class="btn btn-danger fileinput-exists"
                                       data-dismiss="fileinput">@lang('solutions_articles_lang::rubrics.replace.form.image_delete')</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <p class="c-gray m-b-20">@lang('solutions_articles_lang::rubrics.replace.form.tags')</p>
                        {!! Form::text('tags', NULL, ['class'=>'input-sm form-control fg-input']) !!}
                        <small class="help-description">@lang('solutions_articles_lang::rubrics.replace.form.tags_help_description')</small>
                    </div>
                    @if(\PermissionsController::isPackageEnabled('core_seo') && \PermissionsController::allowPermission('solutions_articles', 'seo', FALSE))
                        @include('core_seo_views::seo', ['seo' => $rubric])
                    @endif
                    @if(\PermissionsController::isPackageEnabled('core_open_graph') && \PermissionsController::allowPermission('solutions_articles', 'open_graph', FALSE))
                        @include('core_open_graph_views::open_graph', ['open_graph' => $rubric])
                    @endif
                </div>
                <div class="col-sm-2">
                    <div class="form-group">
                        <p class="c-gray m-b-10">@lang('solutions_articles_lang::rubrics.replace.form.template')</p>
                        {!! Form::select('template_id', $templates, NULL,['class' => 'selectpicker', 'autocomplete' => 'off']) !!}
                    </div>
                    <button type="submit" autocomplete="off" class="btn btn-primary btn-sm m-t-10 waves-effect">
                        <i class="fa fa-save"></i>
                        <span class="btn-text">@lang('solutions_articles_lang::rubrics.replace.form.submit')</span>
                    </button>
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
@stop
@section('scripts_after')
    @set($summernote_locale, \App::getLocale() . '-' . strtoupper(\App::getLocale()))
    {!! Html::script('core/summernote/summernote-' . $summernote_locale . '.js') !!}
    <script>
        $("#rubric_content").summernote({
            height: 250,
            tabsize: 2,
            lang: '{{ $summernote_locale }}',
            toolbar: [
                ['font', ['bold', 'italic', 'underline', 'clear']],
                ['fontsize', ['fontsize']],
                ['para', ['ul', 'ol', 'paragraph']],
                ['height', ['height']],
                ['table', ['table']],
                ['insert', ['link', 'picture']],
                ['view', ['fullscreen', 'codeview']],
            ]
        });
        $('#rubric_content').on('summernote.change', function (we, contents, $editable) {
            $("#rubric_content").html(contents);
            $("#rubric_content").change();
        });
        $("#edit-solutions-article-rubric-form input[name='tags']").SelectizeInput();
        var form = $("#edit-solutions-article-rubric-form");
        BASIC.currentForm = form;
        BASIC.validateOptions.rules = {title: {required: true}};
        BASIC.validateOptions.messages = VALIDATION_MESSAGES.defaulRules;
        $(BASIC.currentForm).validate(BASIC.validateOptions);

        if ($("#seo-container").length) {
            $(document).on('click', '#seo-container .auto-paste', function () {
                var title = $(form).find('input[name="title"]').val().trim();
                $(form).find('input[name="seo_url"]').val(BASIC.transliterate(title)).focus();
                $(form).find('input[name="seo_title"]').val(title).focus();
                $(form).find('textarea[name="seo_description"]').val($(form).find('textarea[name="announce"]').val().trim()).focus();
                $(form).find('input[name="seo_h1"]').val(title).focus();
            })
        }
        if ($("#open-graph-container").length) {
            $(document).on('click', '#open-graph-container .auto-paste', function () {
                $(form).find('input[name="og[og:title]"]').val($(form).find('input[name="title"]').val().trim()).focus();
                $(form).find('textarea[name="og[og:description]"]').val($(form).find('textarea[name="announce"]').val().trim()).focus();
            })
        }
    </script>
@stop