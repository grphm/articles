@extends("core_system_views::layouts.$current_layout")
@section('title', trans('core_system_lang::dashboard.control_panel'))
@section('breadcrumb')
    <ol class="breadcrumb">
        <li>
            <a href="{{ route('dashboard') }}">
                <i class="zmdi zmdi-view-dashboard"></i> @lang('core_system_lang::system.dashboard')
            </a>
        </li>
        <li class="c-gray">
            <i class="{{ config('solutions_articles::menu.icon') }}"></i> {!! array_translate(config('solutions_articles::menu.title')) !!}
        </li>
        <li class="active">
            <i class="{{ config('solutions_articles::menu.menu_child.rubrics.icon') }}"></i> {!! array_translate(config('solutions_articles::menu.menu_child.rubrics.title')) !!}
        </li>
    </ol>
@stop
@section('content')
    <div class="block-header">
        <h2>
            <i class="{{ config('solutions_articles::menu.menu_child.rubrics.icon') }}"></i>
            {!! array_translate(config('solutions_articles::menu.menu_child.rubrics.title')) !!}
        </h2>
    </div>
    @BtnAdd('solutions.articles.rubrics.create')
    <div class="card">
        <div class="list-group lg-odd-black">
            <div class="action-header clearfix">
                <div class="ah-label hidden-xs">
                    @lang('solutions_articles_lang::rubrics.list')
                </div>
                @if($rubrics->count())
                    {!! Form::open(['route' => 'solutions.articles.rubrics.index', 'method' => 'get']) !!}
                    <div class="ah-search">
                        <input type="text" name="search" placeholder="@lang('solutions_articles_lang::rubrics.search')"
                               class="ahs-input">
                        <i class="ahs-close" data-ma-action="action-header-close">&times;</i>
                    </div>
                    {!! Form::close() !!}
                    <ul class="actions">
                        <li>
                            <a href="" data-ma-action="action-header-open">
                                <i class="zmdi zmdi-search"></i>
                            </a>
                        </li>
                        <li class="dropdown">
                            <a href="" data-toggle="dropdown" aria-expanded="false" aria-haspopup="true">
                                <i class="zmdi zmdi-sort-amount-asc"></i>
                            </a>
                            <ul class="dropdown-menu dropdown-menu-right">
                                <li>
                                    <a href="{{ route('solutions.articles.rubrics.index', array_merge(Request::all(), ['sort_field' => 'title', 'sort_direction' => 'asc'])) }}">
                                        @lang('solutions_articles_lang::rubrics.sort_title')
                                    </a>
                                </li>
                                <li>
                                    <a href="{{ route('solutions.articles.rubrics.index', array_merge(Request::all(), ['sort_field' => 'updated_at', 'sort_direction' => 'asc'])) }}">
                                        @lang('solutions_articles_lang::rubrics.sort_updated')
                                    </a>
                                </li>
                            </ul>
                        </li>
                        <li class="dropdown">
                            <a href="" data-toggle="dropdown" aria-expanded="false" aria-haspopup="true">
                                <i class="zmdi zmdi-sort-amount-desc"></i>
                            </a>
                            <ul class="dropdown-menu dropdown-menu-right">
                                <li>
                                    <a href="{{ route('solutions.articles.rubrics.index', array_merge(Request::all(), ['sort_field' => 'title', 'sort_direction' => 'desc'])) }}">
                                        @lang('solutions_articles_lang::rubrics.sort_title')
                                    </a>
                                </li>
                                <li>
                                    <a href="{{ route('solutions.articles.rubrics.index', array_merge(Request::all(), ['sort_field' => 'updated_at', 'sort_direction' => 'desc'])) }}">
                                        @lang('solutions_articles_lang::rubrics.sort_updated')
                                    </a>
                                </li>
                            </ul>
                        </li>
                    </ul>
                @endif
            </div>
            <div class="card-body card-padding m-h-250 p-0">
                @if($rubrics->count())
                    <ul class="nestable-list p-l-0" data-action-url="{!! route('solutions.articles.rubrics.sortable') !!}">
                        @foreach($rubrics as $rubric)
                            <li class="list-group-item media js-item-container js-nestable-list-item" data-element="{!! $rubric->id !!}">
                                <div class="pull-left m-t-5">
                                    <i class="zmdi zmdi-swap-vertical f-20 p-t-10 cursor-move"></i>
                                </div>
                                @if($rubric->announce_image && \Storage::exists($rubric->announce_image))
                                    <div class="pull-left clearfix">
                                        <img alt="{{ $rubric->title }}" class="lgi-img h-50 brd-0"
                                             src="{!! asset('uploads/' . $rubric->announce_image) !!}">
                                    </div>
                                @endif
                                <div class="pull-right">
                                    <div class="actions dropdown">
                                        <a aria-expanded="true" data-toggle="dropdown" href="">
                                            <i class="zmdi zmdi-more-vert"></i>
                                        </a>
                                        <ul class="dropdown-menu dropdown-menu-right">
                                            <li>
                                                <a href="{!! route('solutions.articles.rubrics.edit', $rubric->id) !!}">
                                                    @lang('solutions_articles_lang::rubrics.edit')
                                                </a>
                                            </li>
                                            <li>
                                                <a href="{!! \PublicArticle::makeLocaleURL(\App::getLocale(), $rubric) !!}" target="_blank">
                                                    @lang('core_content_lang::pages.blank')
                                                </a>
                                            </li>
                                            <li class="divider"></li>
                                            <li>
                                                <a class="c-red js-item-remove" href="">
                                                    @lang('solutions_articles_lang::rubrics.delete.submit')
                                                </a>
                                                {!! Form::open(['route' => ['solutions.articles.rubrics.destroy', $rubric->id], 'method' => 'DELETE', 'class' => 'hidden']) !!}
                                                <button type="submit"
                                                        data-question="@lang('solutions_articles_lang::rubrics.delete.question') &laquo;{{ $rubric->title }}&raquo;?"
                                                        data-confirmbuttontext="@lang('solutions_articles_lang::rubrics.delete.confirmbuttontext')"
                                                        data-cancelbuttontext="@lang('solutions_articles_lang::rubrics.delete.cancelbuttontext')">
                                                </button>
                                                {!! Form::close() !!}
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="media-body">
                                    <div class="lgi-heading">{{ $rubric->title }}</div>
                                    <small class="lgi-text">{{ $rubric->announce }}</small>
                                    <ul class="lgi-attrs">
                                        <li>@lang('solutions_articles_lang::rubrics.slug'): {!! $rubric->slug !!}</li>
                                        <li>@lang('solutions_articles_lang::rubrics.template'): {!! $rubric->template->title !!}</li>
                                        <li>
                                            @lang('solutions_articles_lang::rubrics.update'):
                                            {!! $rubric->UpdatedDate !!}
                                        </li>
                                    </ul>
                                </div>
                            </li>
                        @endforeach
                    </ul>
                @else
                    <h2 class="f-16 c-gray m-l-30">@lang('solutions_articles_lang::rubrics.empty')</h2>
                @endif
            </div>
        </div>
    </div>
@stop