@extends("core_system_views::layouts.$current_layout")
@section('title', trans('core_system_lang::dashboard.control_panel'))
@section('breadcrumb')
    <ol class="breadcrumb">
        <li>
            <a href="{{ route('dashboard') }}">
                <i class="zmdi zmdi-view-dashboard"></i> @lang('core_system_lang::system.dashboard')
            </a>
        </li>
        <li>
            <a href="{{ route('solutions.articles.index') }}">
                <i class="{{ config('solutions_articles::menu.icon') }}"></i> {!! array_translate(config('solutions_articles::menu.title')) !!}
            </a>
        </li>
        <li class="c-gray">
            <i class="zmdi zmdi-edit"></i> @lang('solutions_articles_lang::articles.replace.breadcrumb')
        </li>
        <li class="active">
            <i class="zmdi zmdi-edit"></i> @lang('solutions_articles_lang::articles.blocks_edit')
        </li>
    </ol>
@stop
@section('content')
    <div class="block-header">
        <h2><i class="zmdi zmdi-edit"></i> @lang('solutions_articles_lang::articles.blocks_edit')</h2>
    </div>
    <div class="card">
        <div class="action-header clearfix">
            <div class="ah-label hidden-xs">
                {!! $article->title !!}
            </div>
        </div>
        <div class="card-body card-padding">
            {!! Form::open(['route' => ['solutions.articles.blocks_update', $article->id], 'class' => 'form-validate', 'id' => 'edit-solutions-article-blocks-form', 'method' => 'PUT']) !!}
            <div class="row">
                <div class="col-sm-12">
                    @foreach($blocks as $index => $block)
                        <div{!! $index ? ' class="m-t-25"' : '' !!}>
                            <p class="f-500 c-black m-b-5">{!! $block->template->title !!}</p>
                            <small>
                                @lang('solutions_articles_lang::templates.root_directory')
                                /{!! $block->template->path !!}
                            </small>
                            <br>
                            {!! Form::hidden('blocks[]', $block->id) !!}
                            {!! Form::textarea('contents[]', $block->content, ['class' => 'redactor']) !!}
                        </div>
                    @endforeach
                </div>
            </div>
            <div class="row">
                <div class="col-sm-12">
                    <button type="submit" autocomplete="off" class="btn btn-primary btn-sm m-t-10 waves-effect">
                        <i class="fa fa-save"></i>
                        <span class="btn-text">@lang('solutions_articles_lang::articles.replace.form.submit')</span>
                    </button>
                </div>
            </div>
            {!! Form::close() !!}
        </div>
    </div>
@stop
@section('scripts_after')
    @set($summernote_locale, \App::getLocale() . '-' . strtoupper(\App::getLocale()))
    {!! Html::script('core/summernote/summernote-' . $summernote_locale . '.js') !!}
    <script>
        $(".redactor").summernote({
            height: 250, tabsize: 2, lang: '{!! $summernote_locale !!}',
            toolbar: [
                ['font', ['bold', 'italic', 'underline', 'clear']],
                ['fontsize', ['fontsize']],
                ['para', ['ul', 'ol', 'paragraph']],
                ['height', ['height']],
                ['table', ['table']],
                ['insert', ['link', 'picture']],
                ['view', ['fullscreen', 'codeview']],
            ]
        });
        $('.redactor').on('summernote.change', function (we, contents, $editable) {
            $(this).html(contents);
            $(this).change();
        });
        BASIC.currentForm = $("#edit-solutions-article-blocks-form");
        BASIC.validateOptions.rules = {};
        BASIC.validateOptions.messages = {};
        $(BASIC.currentForm).validate(BASIC.validateOptions);
    </script>
@stop