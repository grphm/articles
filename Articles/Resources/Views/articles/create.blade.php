@extends("core_system_views::layouts.$current_layout")
@section('title', trans('core_system_lang::dashboard.control_panel'))
@section('breadcrumb')
    <ol class="breadcrumb">
        <li>
            <a href="{{ route('dashboard') }}">
                <i class="zmdi zmdi-view-dashboard"></i> @lang('core_system_lang::system.dashboard')
            </a>
        </li>
        <li>
            <a href="{{ route('solutions.articles.index') }}">
                <i class="{{ config('solutions_articles::menu.icon') }}"></i> {!! array_translate(config('solutions_articles::menu.title')) !!}
            </a>
        </li>
        <li class="active">
            <i class="zmdi zmdi-plus"></i> @lang('solutions_articles_lang::articles.insert.breadcrumb')
        </li>
    </ol>
@stop
@section('content')
    <div class="block-header">
        <h2><i class="zmdi zmdi-plus"></i> @lang('solutions_articles_lang::articles.insert.title')</h2>
    </div>
    <div class="card">
        <div class="card-body card-padding">
            <div class="row">
                {!! Form::open(['route' => 'solutions.articles.store', 'class' => 'form-validate', 'id' => 'add-solutions-article-form']) !!}
                <div class="col-sm-9">
                    <div class="form-group fg-float">
                        <div class="fg-line">
                            {!! Form::text('title', NULL, ['class'=>'input-sm form-control fg-input']) !!}
                            <label class="fg-label">@lang('solutions_articles_lang::articles.insert.form.title')</label>
                        </div>
                    </div>
                    <div class="form-group fg-float">
                        <div class="fg-line">
                            {!! Form::textarea('announce', NULL, ['class' => 'form-control auto-size fg-input', 'data-autosize-on' => 'true', 'rows' => 1]) !!}
                            <label class="fg-label">@lang('solutions_articles_lang::articles.insert.form.announce')</label>
                        </div>
                    </div>
                    <div class="form-group">
                        <p class="c-gray m-b-20">@lang('solutions_articles_lang::articles.insert.form.content')</p>
                        @if(!empty($blocks))
                            <ul class="clist clist-angle js-article-content-structure">
                                @foreach($blocks as $block_id => $block_title)
                                    <li class="f-14 m-t-10">
                                        <input type="hidden" name="content[]" value="{!! $block_id !!}">
                                        {!! $block_title !!}
                                        <a class="c-gray js-article-block-remove" href=""><i
                                                    class="zmdi zmdi-minus-circle-outline"></i></a>
                                    </li>
                                    @break
                                @endforeach
                            </ul>
                            <div id="js-article-block-templates-list" class="dropdown m-t-20">
                                <button type="button" class="btn btn-default waves-effect" data-toggle="dropdown"
                                        aria-expanded="false">
                                    <i class="zmdi zmdi-plus"></i>
                                    @lang('solutions_articles_lang::articles.insert.form.block_add')
                                </button>
                                <ul class="dropdown-menu">
                                    @foreach($blocks as $block_id => $block_title)
                                        <li>
                                            <a data-block-id="{!! $block_id !!}" class="js-article-block-template"
                                               href="">
                                                {!! $block_title !!}
                                            </a>
                                        </li>
                                    @endforeach
                                </ul>
                            </div>
                        @else
                            <span class="badge bgm-orange f-12 p-10">
                                <i class="zmdi zmdi-alert-circle-o"></i>
                                @lang('solutions_articles_lang::articles.insert.form.block_templates_missing').
                                <a class="m-l-5 c-black" href="{!! route('solutions.articles.templates.index') !!}">
                                    @lang('solutions_articles_lang::articles.insert.form.block_templates_create')
                                </a>
                            </span>
                        @endif
                    </div>
                    <div class="row">
                        <div class="col-sm-4">
                            <p class="c-gray m-b-5">@lang('solutions_articles_lang::articles.insert.form.main_image')</p>
                            <small class="help-description">@lang('solutions_articles_lang::articles.insert.form.main_image_help_description')</small>
                            <div class="clearfix"></div>
                            <div class="fileinput fileinput-new m-t-5" data-provides="fileinput">
                                <div class="fileinput-preview thumbnail" data-trigger="fileinput"></div>
                                <div>
                                    <div class="btn btn-info btn-file">
                                        <span class="fileinput-new">@lang('solutions_articles_lang::articles.insert.form.image_select')</span>
                                        <span class="fileinput-exists">@lang('solutions_articles_lang::articles.insert.form.image_change')</span>
                                        {!! Form::file('main_image') !!}
                                    </div>
                                    <a href="#" class="btn btn-danger fileinput-exists"
                                       data-dismiss="fileinput">@lang('solutions_articles_lang::articles.insert.form.image_delete')</a>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <p class="c-gray m-b-5">@lang('solutions_articles_lang::articles.insert.form.announce_image')</p>
                            <small class="help-description">@lang('solutions_articles_lang::articles.insert.form.announce_image_help_description')</small>
                            <div class="clearfix"></div>
                            <div class="fileinput fileinput-new m-t-5" data-provides="fileinput">
                                <div class="fileinput-preview thumbnail" data-trigger="fileinput"></div>
                                <div>
                                    <div class="btn btn-info btn-file">
                                        <span class="fileinput-new">@lang('solutions_articles_lang::articles.insert.form.image_select')</span>
                                        <span class="fileinput-exists">@lang('solutions_articles_lang::articles.insert.form.image_change')</span>
                                        {!! Form::file('announce_image') !!}
                                    </div>
                                    <a href="#" class="btn btn-danger fileinput-exists"
                                       data-dismiss="fileinput">@lang('solutions_articles_lang::articles.insert.form.image_delete')</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <p class="c-gray m-b-20">@lang('solutions_articles_lang::articles.insert.form.tags')</p>
                        {!! Form::text('tags', NULL, ['class'=>'input-sm form-control fg-input']) !!}
                        <small class="help-description">@lang('solutions_articles_lang::articles.insert.form.tags_help_description')</small>
                    </div>
                    @if(\PermissionsController::isPackageEnabled('core_seo') && \PermissionsController::allowPermission('solutions_articles', 'seo', FALSE))
                        @include('core_seo_views::seo')
                    @endif
                    @if(\PermissionsController::isPackageEnabled('core_open_graph') && \PermissionsController::allowPermission('solutions_articles', 'open_graph', FALSE))
                        @include('core_open_graph_views::open_graph')
                    @endif
                </div>
                <div class="col-sm-3">
                    <div class="form-group">
                        <p class="c-gray m-b-10">@lang('solutions_articles_lang::articles.insert.form.rubric')</p>
                        {!! Form::select('rubric_id', \PublicArticle::getRubricsList(), NULL,['class' => 'selectpicker', 'autocomplete' => 'off']) !!}
                    </div>
                    <div class="form-group">
                        <p class="c-gray m-b-10">@lang('solutions_articles_lang::articles.insert.form.template')</p>
                        {!! Form::select('template_id', $templates, NULL,['class' => 'selectpicker', 'autocomplete' => 'off']) !!}
                    </div>
                    <div class="form-group fg-float">
                        <div class="fg-line">
                            {!! Form::text('published_at', NULL, ['class' => 'input-sm form-control fg-input date-picker date-mask text-center']) !!}
                            <label class="fg-label">@lang('solutions_articles_lang::articles.insert.form.published_at')</label>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="checkbox">
                            <label>
                                {!! Form::checkbox('publication', TRUE, FALSE, ['autocomplete' => 'off']) !!}
                                <i class="input-helper"></i> @lang('solutions_articles_lang::articles.insert.form.publish')
                            </label>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="checkbox">
                            <label>
                                {!! Form::checkbox('top', TRUE, FALSE, ['autocomplete' => 'off']) !!}
                                <i class="input-helper"></i> @lang('solutions_articles_lang::articles.insert.form.top')
                            </label>
                        </div>
                    </div>
                    <button type="submit" autocomplete="off" class="btn btn-primary btn-sm m-t-10 waves-effect">
                        <i class="fa fa-save"></i>
                        <span class="btn-text">@lang('solutions_articles_lang::articles.insert.form.submit')</span>
                    </button>
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
@stop
@section('scripts_after')
    <script>
        $("#add-solutions-article-form input[name='tags']").SelectizeInput();
        var form = $("#add-solutions-article-form");
        BASIC.currentForm = form;
        BASIC.validateOptions.rules = {title: {required: true}, published_at: {required: true}};
        BASIC.validateOptions.messages = VALIDATION_MESSAGES.defaulRules;
        $(BASIC.currentForm).validate(BASIC.validateOptions);
        $(".js-article-block-template").click(function (event) {
            event.preventDefault();
            var block_id = $(this).data('block-id');
            var block_title = $(this).html();
            $(".js-article-content-structure").append('<li class="f-14 m-t-10"><input type="hidden" name="content[]" value="' + block_id + '">' + block_title + '<a class="c-gray js-article-block-remove" href=""><i class="zmdi zmdi-minus-circle-outline"></i></a></li>');
            $("#js-article-block-templates-list").removeClass('open');
        });
        $(document).on('click', '.js-article-block-remove', function (event) {
            event.preventDefault();
            $(this).parent().remove();
        });
        if ($("#seo-container").length) {
            $(document).on('click', '#seo-container .auto-paste', function () {
                var title = $(form).find('input[name="title"]').val().trim();
                $(form).find('input[name="seo_url"]').val(BASIC.transliterate(title)).focus();
                $(form).find('input[name="seo_title"]').val(title).focus();
                $(form).find('textarea[name="seo_description"]').val($(form).find('textarea[name="announce"]').val().trim()).focus();
                $(form).find('input[name="seo_h1"]').val(title).focus();
            })
        }
        if ($("#open-graph-container").length) {
            $(document).on('click', '#open-graph-container .auto-paste', function () {
                $(form).find('input[name="og[og:title]"]').val($(form).find('input[name="title"]').val().trim()).focus();
                $(form).find('textarea[name="og[og:description]"]').val($(form).find('textarea[name="announce"]').val().trim()).focus();
            })
        }
    </script>
@stop