@extends("core_system_views::layouts.$current_layout")
@section('title', trans('core_system_lang::dashboard.control_panel'))
@section('breadcrumb')
    <ol class="breadcrumb">
        <li>
            <a href="{{ route('dashboard') }}">
                <i class="zmdi zmdi-view-dashboard"></i> @lang('core_system_lang::system.dashboard')
            </a>
        </li>
        <li class="active">
            <i class="{{ config('solutions_articles::menu.icon') }}"></i> {!! array_translate(config('solutions_articles::menu.title')) !!}
        </li>
    </ol>
@stop
@section('content')
    <div class="block-header">
        <h2>
            <i class="{{ config('solutions_articles::menu.icon') }}"></i>
            {!! array_translate(config('solutions_articles::menu.title')) !!}
        </h2>
    </div>
    @if(\PermissionsController::allowPermission('solutions_articles', 'create', FALSE))
        @BtnAdd('solutions.articles.create')
    @endif
    <div class="card">
        <div class="list-group lg-odd-black">
            <div class="action-header clearfix">
                <div class="ah-label hidden-xs">
                    @if(\Request::has('rubric') === FALSE)
                        @lang('solutions_articles_lang::articles.all_rubrics'):
                    @elseif(!is_null($rubric))
                        {!! $rubric->title !!}:
                    @endif
                    @lang('solutions_articles_lang::articles.list')
                </div>
                {!! Form::open(['route' => 'solutions.articles.index', 'method' => 'get']) !!}
                <div class="ah-search">
                    <input type="text" name="search" placeholder="@lang('solutions_articles_lang::articles.search')"
                           class="ahs-input">
                    <i class="ahs-close" data-ma-action="action-header-close">&times;</i>
                </div>
                {!! Form::close() !!}
                <ul class="actions">
                    <li>
                        <a href="" data-ma-action="action-header-open">
                            <i class="zmdi zmdi-search"></i>
                        </a>
                    </li>
                    @if($articles->count())
                        <li class="dropdown">
                            <a href="" data-toggle="dropdown" aria-expanded="false" aria-haspopup="true">
                                <i class="zmdi zmdi-sort-amount-asc"></i>
                            </a>
                            <ul class="dropdown-menu dropdown-menu-right">
                                <li>
                                    <a href="{{ route('solutions.articles.index', array_merge(Request::all(), ['sort_field' => 'title', 'sort_direction' => 'asc'])) }}">
                                        @lang('solutions_articles_lang::articles.sort_title')
                                    </a>
                                </li>
                                <li>
                                    <a href="{{ route('solutions.articles.index', array_merge(Request::all(), ['sort_field' => 'created_at', 'sort_direction' => 'asc'])) }}">
                                        @lang('solutions_articles_lang::articles.sort_published')
                                    </a>
                                </li>
                                <li>
                                    <a href="{{ route('solutions.articles.index', array_merge(Request::all(), ['sort_field' => 'updated_at', 'sort_direction' => 'asc'])) }}">
                                        @lang('solutions_articles_lang::articles.sort_updated')
                                    </a>
                                </li>
                            </ul>
                        </li>
                        <li class="dropdown">
                            <a href="" data-toggle="dropdown" aria-expanded="false" aria-haspopup="true">
                                <i class="zmdi zmdi-sort-amount-desc"></i>
                            </a>
                            <ul class="dropdown-menu dropdown-menu-right">
                                <li>
                                    <a href="{{ route('solutions.articles.index', array_merge(Request::all(), ['sort_field' => 'title', 'sort_direction' => 'desc'])) }}">
                                        @lang('solutions_articles_lang::articles.sort_title')
                                    </a>
                                </li>
                                <li>
                                    <a href="{{ route('solutions.articles.index', array_merge(Request::all(), ['sort_field' => 'created_at', 'sort_direction' => 'desc'])) }}">
                                        @lang('solutions_articles_lang::articles.sort_published')
                                    </a>
                                </li>
                                <li>
                                    <a href="{{ route('solutions.articles.index', array_merge(Request::all(), ['sort_field' => 'updated_at', 'sort_direction' => 'desc'])) }}">
                                        @lang('solutions_articles_lang::articles.sort_updated')
                                    </a>
                                </li>
                            </ul>
                        </li>
                    @endif
                    <li class="dropdown">
                        <a href="" data-toggle="dropdown" aria-expanded="true">
                            <i class="zmdi zmdi-filter-list"></i>
                        </a>
                        <ul class="dropdown-menu dropdown-menu-right">
                            <li>
                                <a href="{{ route('solutions.articles.index') }}">@lang('solutions_articles_lang::articles.all_rubrics')</a>
                            </li>
                            <li class="divider"></li>
                            @foreach(\STALKER_CMS\Solutions\Articles\Models\Rubric::whereLocale(\App::getLocale())->orderBy('order')->pluck('title', 'slug') as $slug => $title)
                                <li>
                                    <a href="{!! route('solutions.articles.index') . '?rubric=' . $slug !!}">
                                        {!! $title !!}
                                    </a>
                                </li>
                            @endforeach
                        </ul>
                    </li>
                </ul>
            </div>
            <div class="card-body card-padding m-h-250 p-0">
                @forelse($articles as $article)
                    <div class="js-item-container list-group-item media">
                        @if($article->announce_image && \Storage::exists($article->announce_image))
                            <div class="pull-left clearfix">
                                <img alt="{{ $article->title }}" class="lgi-img" src="{!! $article->AssetImage !!}">
                            </div>
                        @endif
                        <div class="pull-right">
                            <div class="actions dropdown">
                                <a aria-expanded="true" data-toggle="dropdown" href="">
                                    <i class="zmdi zmdi-more-vert"></i>
                                </a>
                                <ul class="dropdown-menu dropdown-menu-right">
                                    @if(\PermissionsController::allowPermission('solutions_articles', 'edit', FALSE))
                                        <li>
                                            <a href="{!! route('solutions.articles.edit', $article->id) !!}">
                                                @lang('solutions_articles_lang::articles.edit')
                                            </a>
                                        </li>
                                        <li>
                                            <a href="{!! route('solutions.articles.blocks_edit', $article->id) !!}">
                                                @lang('solutions_articles_lang::articles.blocks_edit')
                                            </a>
                                        </li>
                                    @endif
                                    @if($article->publication)
                                        <li>
                                            <a href="{!! \PublicArticle::makeLocaleURL(\App::getLocale(), $article) !!}"
                                               target="_blank">
                                                @lang('solutions_articles_lang::articles.blank')
                                            </a>
                                        </li>
                                    @endif
                                    @if(\PermissionsController::allowPermission('solutions_articles', 'delete', FALSE))
                                        <li class="divider"></li>
                                        <li>
                                            <a class="c-red js-item-remove" href="">
                                                @lang('solutions_articles_lang::articles.delete.submit')
                                            </a>
                                            {!! Form::open(['route' => ['solutions.articles.destroy', $article->id], 'method' => 'DELETE', 'class' => 'hidden']) !!}
                                            <button type="submit"
                                                    data-question="@lang('solutions_articles_lang::articles.delete.question') &laquo;{{ $article->title }}&raquo;?"
                                                    data-confirmbuttontext="@lang('solutions_articles_lang::articles.delete.confirmbuttontext')"
                                                    data-cancelbuttontext="@lang('solutions_articles_lang::articles.delete.cancelbuttontext')">
                                            </button>
                                            {!! Form::close() !!}
                                        </li>
                                    @endif
                                </ul>
                            </div>
                        </div>
                        <div class="media-body">
                            <div class="lgi-heading">{{ $article->title }}</div>
                            <small class="lgi-text">{{ $article->announce }}</small>
                            <ul class="lgi-attrs">
                                <li>
                                    @lang('solutions_articles_lang::articles.rubric'):
                                    {!! !empty($article->rubric) ? $article->rubric->title : \Lang::get('solutions_articles_lang::articles.rubric_empty') !!}
                                </li>
                                <li>
                                    @lang('solutions_articles_lang::articles.published'):
                                    {!! $article->PublishedDate !!}
                                </li>
                                <li>
                                    @lang('solutions_articles_lang::articles.in_top.title'):
                                    {!! $article->InTop !!}
                                </li>
                                <li>
                                    @lang('solutions_articles_lang::articles.author'):
                                    {!! is_object($article->author) ? $article->author->name : '<span class="c-red">'.trans('root_lang::codes.410').'<span>' !!}
                                </li>
                                <li>
                                    @lang('solutions_articles_lang::articles.update'):
                                    {!! $article->UpdatedDate !!}
                                </li>
                            </ul>
                        </div>
                    </div>
                @empty
                    <h2 class="f-16 c-gray m-l-30">@lang('solutions_articles_lang::articles.empty')</h2>
                @endforelse
                <div class="lg-pagination p-10">
                    {!! $articles->appends(\Request::only(['search', 'sort_field', 'sort_direction']))->render() !!}
                </div>
            </div>
        </div>
    </div>
@stop