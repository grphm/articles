<?php
return [
    'all_types' => 'Все типы',
    'templates' => 'Artículo de la rubrica de plantillas',
    'root_directory' => 'Directorio raíz: /home/Resources/Views/solutions/articles',
    'sort_date_created' => 'Fecha de recepción',
    'empty' => 'Lista está vacía',
    'edit' => 'Editar',
    'delete' => [
        'question' => 'Eliminar plantilla',
        'confirmbuttontext' => 'Sí, eliminar',
        'cancelbuttontext' => 'He cambiado de idea',
        'submit' => 'Eliminar',
    ],
    'empty' => 'Lista está vacía',
    'insert' => [
        'breadcrumb' => 'Añadir',
        'title' => 'Adición de la plantilla',
        'form' => [
            'content' => 'Plantilla de contenido',
            'template_type' => 'Tipo de plantilla',
            'title' => 'Título',
            'title_help_description' => 'Por ejemplo: Rúbrica principal',
            'path' => 'Nombre del archivo de plantilla',
            'path_help_description' => '.blade.php en expansión no es necesaria <br> Ejemplo: main',
            'submit' => 'Guardar'
        ]
    ],
    'replace' => [
        'breadcrumb' => 'Editar',
        'title' => 'Editar plantilla',
        'form' => [
            'content' => 'Plantilla de contenido',
            'title' => 'Título',
            'title_help_description' => 'Por ejemplo: Rúbrica principal',
            'submit' => 'Guardar'
        ]
    ]
];