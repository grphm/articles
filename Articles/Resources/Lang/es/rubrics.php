<?php
return [
    'list' => 'Lista de rúbricas',
    'search' => 'Introduzca el título del rúbrica',
    'sort_title' => 'Título',
    'sort_updated' => 'Fecha de actualización',
    'edit' => 'Editar',
    'blank' => 'Abrir en nueva ventana',
    'empty' => 'Lista está vacía',
    'slug' => 'Simbólico código',
    'template' => 'Plantilla',
    'update' => 'Fecha de actualización',
    'delete' => [
        'question' => 'Eliminar rúbrica',
        'confirmbuttontext' => 'Sí, eliminar',
        'cancelbuttontext' => 'He cambiado de idea',
        'submit' => 'Eliminar',
    ],
    'insert' => [
        'breadcrumb' => 'Añadir',
        'title' => 'Agregar rúbrica',
        'form' => [
            'slug' => 'Simbólico código',
            'slug_help_description' => 'Sólo latino personajes, guiones, guiones, al menos 5 caracteres. <br> Ejemplo: main_rubric',
            'template' => 'Plantilla de la rúbrica',
            'title' => 'Título',
            'announce' => 'Anuncio',
            'content' => 'Contenido',
            'main_image' => 'Imagen principal',
            'main_image_help_description' => 'Formatos soportados: png, jpg, gif',
            'announce_image' => 'Imagen del anuncio',
            'announce_image_help_description' => 'Formatos soportados: png, jpg, gif',
            'image_select' => 'Selecto',
            'image_change' => 'Enmendar',
            'image_delete' => 'Eliminar',
            'tags' => 'Etiquetas',
            'tags_help_description' => 'Lista separada por comas',
            'submit' => 'Guardar'
        ]
    ],
    'replace' => [
        'breadcrumb' => 'Editar',
        'title' => 'Editar artículo',
        'form' => [
            'template' => 'Plantilla de la rúbrica',
            'title' => 'Título',
            'announce' => 'Anuncio',
            'content' => 'Contenido',
            'main_image' => 'Imagen principal',
            'main_image_help_description' => 'Formatos soportados: png, jpg, gif',
            'announce_image' => 'Imagen del anuncio',
            'announce_image_help_description' => 'Formatos soportados: png, jpg, gif',
            'image_select' => 'Selecto',
            'image_change' => 'Enmendar',
            'image_delete' => 'Eliminar',
            'tags' => 'Etiquetas',
            'tags_help_description' => 'Lista separada por comas',
            'submit' => 'Guardar'
        ]
    ]
];