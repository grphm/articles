<?php
return [
    'list' => 'List of rubrics',
    'search' => 'Enter the title of the rubric',
    'sort_title' => 'Title',
    'sort_updated' => 'Update date',
    'edit' => 'Edit',
    'blank' => 'Open in new window',
    'empty' => 'List is empty',
    'slug' => 'Symbolic code',
    'template' => 'Template',
    'update' => 'Update date',
    'delete' => [
        'question' => 'Delete rubric',
        'confirmbuttontext' => 'Yes, delete',
        'cancelbuttontext' => 'No, I change my mind',
        'submit' => 'Delete',
    ],
    'insert' => [
        'breadcrumb' => 'Add',
        'title' => 'Adding rubric',
        'form' => [
            'slug' => 'Symbolic code',
            'slug_help_description' => 'Only latin characters, underscores, dashes, at least 5 characters. <br> Example: main_rubric',
            'template' => 'Template of rubric',
            'title' => 'Title',
            'announce' => 'Announce',
            'content' => 'Content',
            'main_image' => 'Main picture',
            'main_image_help_description' => 'Supported formats: png, jpg, gif',
            'announce_image' => 'Image of announcement',
            'announce_image_help_description' => 'Supported formats: png, jpg, gif',
            'image_select' => 'Select',
            'image_change' => 'Change',
            'image_delete' => 'Delete',
            'tags' => 'Tags',
            'tags_help_description' => 'List separated by commas',
            'submit' => 'Save'
        ]
    ],
    'replace' => [
        'breadcrumb' => 'Edit',
        'title' => 'Edit article',
        'form' => [
            'template' => 'Template of rubric',
            'title' => 'Title',
            'announce' => 'Announce',
            'content' => 'Content',
            'main_image' => 'Main picture',
            'main_image_help_description' => 'Supported formats: png, jpg, gif',
            'announce_image' => 'Image of announcement',
            'announce_image_help_description' => 'Supported formats: png, jpg, gif',
            'image_select' => 'Select',
            'image_change' => 'Change',
            'image_delete' => 'Delete',
            'tags' => 'Tags',
            'tags_help_description' => 'List separated by commas',
            'submit' => 'Save'
        ]
    ]
];