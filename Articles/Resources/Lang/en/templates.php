<?php
return [
    'all_types' => 'Все типы',
    'templates' => 'Templates rubric article',
    'root_directory' => 'Root directory: /home/Resources/Views/solutions/articles',
    'sort_date_created' => 'Date received',
    'empty' => 'List is empty',
    'edit' => 'Edit',
    'delete' => [
        'question' => 'Delete template',
        'confirmbuttontext' => 'Yes, delete',
        'cancelbuttontext' => 'No, I change my mind',
        'submit' => 'Delete',
    ],
    'empty' => 'List is empty',
    'insert' => [
        'breadcrumb' => 'Add',
        'title' => 'Adding template',
        'form' => [
            'content' => 'Content template',
            'template_type' => 'Type of template',
            'title' => 'Title',
            'title_help_description' => 'For example: Main rubric',
            'path' => 'Filename template',
            'path_help_description' => 'Expanding .blade.php is not needed!<br> Example: main',
            'submit' => 'Save'
        ]
    ],
    'replace' => [
        'breadcrumb' => 'Edit',
        'title' => 'Edit template',
        'form' => [
            'content' => 'Content template',
            'title' => 'Title',
            'title_help_description' => 'For example: Main rubric',
            'submit' => 'Save'
        ]
    ]
];