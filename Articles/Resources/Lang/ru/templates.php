<?php
return [
    'all_types' => 'Все типы',
    'templates' => 'Шаблоны рубрик статей',
    'root_directory' => 'Корневой каталог: /home/Resources/Views/solutions/articles',
    'sort_date_created' => 'Дата создания',
    'empty' => 'Список пустой',
    'edit' => 'Редактировать',
    'delete' => [
        'question' => 'Удалить шаблон',
        'confirmbuttontext' => 'Да, удалить',
        'cancelbuttontext' => 'Нет, я передумал',
        'submit' => 'Удалить',
    ],
    'empty' => 'Список пустой',
    'insert' => [
        'breadcrumb' => 'Добавить',
        'title' => 'Добавление шаблона',
        'form' => [
            'content' => 'Контент шаблона',
            'template_type' => 'Тип шаблона',
            'title' => 'Название',
            'title_help_description' => 'Например: Основная рубрика',
            'path' => 'Имя файла шаблона',
            'path_help_description' => 'Расширение .blade.php указывать не нужно!<br>Например: main',
            'submit' => 'Сохранить'
        ]
    ],
    'replace' => [
        'breadcrumb' => 'Редактировать',
        'title' => 'Редактирование шаблона',
        'form' => [
            'content' => 'Контент шаблона',
            'title' => 'Название',
            'title_help_description' => 'Например: Основная рубрика',
            'submit' => 'Сохранить'
        ]
    ]
];