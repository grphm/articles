<?php
return [
    'list' => 'Список рубрик',
    'search' => 'Введите название рубрики',
    'sort_title' => 'Название',
    'sort_updated' => 'Дата обновления',
    'edit' => 'Редактировать',
    'blank' => 'Открыть в новом окне',
    'empty' => 'Список пустой',
    'slug' => 'Символьный код',
    'template' => 'Шаблон',
    'update' => 'Обновлена',
    'delete' => [
        'question' => 'Удалить рубрику',
        'confirmbuttontext' => 'Да, удалить',
        'cancelbuttontext' => 'Нет, я передумал',
        'submit' => 'Удалить',
    ],
    'insert' => [
        'breadcrumb' => 'Добавить',
        'title' => 'Добавление рубрики',
        'form' => [
            'slug' => 'Символьный код',
            'slug_help_description' => 'Только латинские символы, символы подчеркивания, тире, не менее 5 символов. <br>Например: main_rubric',
            'template' => 'Шаблон вывода рубрики',
            'title' => 'Название',
            'announce' => 'Анонс',
            'content' => 'Содержание',
            'main_image' => 'Основное изображение',
            'main_image_help_description' => 'Поддерживаемые форматы: png, jpg, gif',
            'announce_image' => 'Изображение анонса',
            'announce_image_help_description' => 'Поддерживаемые форматы: png, jpg, gif',
            'image_select' => 'Выбрать',
            'image_change' => 'Изменить',
            'image_delete' => 'Удалить',
            'tags' => 'Теги',
            'tags_help_description' => 'Перечислить через запятую',
            'submit' => 'Сохранить'
        ]
    ],
    'replace' => [
        'breadcrumb' => 'Редактировать',
        'title' => 'Редактирование рубрики',
        'form' => [
            'template' => 'Шаблон вывода рубрики',
            'title' => 'Название',
            'announce' => 'Анонс',
            'content' => 'Содержание',
            'main_image' => 'Основное изображение',
            'main_image_help_description' => 'Поддерживаемые форматы: png, jpg, gif',
            'announce_image' => 'Изображение анонса',
            'announce_image_help_description' => 'Поддерживаемые форматы: png, jpg, gif',
            'image_select' => 'Выбрать',
            'image_change' => 'Изменить',
            'image_delete' => 'Удалить',
            'tags' => 'Теги',
            'tags_help_description' => 'Перечислить через запятую',
            'submit' => 'Сохранить'
        ]
    ]
];