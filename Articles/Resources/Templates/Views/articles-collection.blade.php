@extends('home_views::layout')
@metaTitle()
@metaDescription()
@section('content')
    @set($articles, \PublicArticle::getArticles())
    <ul>
        @foreach($articles as $article)
            <li>
                <a href="{!! route('public.articles.show', $article->PageUrl) !!}">{{ $article->title }}</a>
                <p>
                    {{ $article->announce }}
                </p>
            </li>
        @endforeach
    </ul>
@stop