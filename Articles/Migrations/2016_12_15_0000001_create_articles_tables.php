<?php
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateArticlesTables extends Migration {

    public function up() {

        Schema::create('solution_articles', function(Blueprint $table) {

            $table->increments('id');
            $table->string('locale', 10)->nullable()->index();
            $table->smallInteger('rubric_id', FALSE, TRUE)->nullable()->index();
            $table->smallInteger('template_id', FALSE, TRUE)->nullable()->index();
            $table->timestamp('published_at')->nullable();
            $table->boolean('publication')->default(1)->nullable;
            $table->string('title', 255)->nullable();
            $table->text('announce')->nullable();
            $table->string('structure')->nullable();
            $table->string('main_image', 100)->nullable();
            $table->string('announce_image', 100)->nullable();
            $table->string('tags', 255)->nullable();
            $table->string('seo_title', 255)->nullable();
            $table->text('seo_description')->nullable();
            $table->string('seo_keywords', 255)->nullable();
            $table->string('seo_h1', 255)->nullable();
            $table->string('seo_url', 255)->nullable()->index();
            $table->text('open_graph')->nullable();
            $table->integer('user_id', FALSE, TRUE)->nullable()->index();
            $table->integer('views', FALSE, TRUE)->default(0)->nullable()->index();
            $table->boolean('top')->default(0)->unsigned()->nullable();
            $table->timestamp('created_at')->nullable();
            $table->timestamp('updated_at')->nullable();
        });
    }

    public function down() {

        Schema::dropIfExists('solution_articles');
    }
}

