<?php
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateArticlesContentBlocksTables extends Migration {

    public function up() {

        Schema::create('solution_articles_content_blocks', function(Blueprint $table) {

            $table->increments('id');
            $table->integer('article_id', FALSE, TRUE)->nullable()->index();
            $table->smallInteger('template_id', FALSE, TRUE)->nullable()->index();
            $table->mediumText('content')->nullable();
            $table->integer('user_id', FALSE, TRUE)->nullable()->index();
            $table->timestamp('created_at')->nullable();
            $table->timestamp('updated_at')->nullable();
        });
    }

    public function down() {

        Schema::dropIfExists('solution_articles_content_blocks');
    }
}

