<?php
return [
    'package_name' => 'solutions_articles',
    'package_title' => ['ru' => 'Статьи', 'en' => 'Articles', 'es' => 'Artículos'],
    'package_icon' => 'zmdi zmdi-receipt',
    'relations' => ['core_content'],
    'package_description' => [
        'ru' => 'Позволяет управлять статьями. Используется модуль "Контент" и "Галереи"',
        'en' => 'It allows you to manage articles. Using the module "Content"',
        'es' => 'Se le permite gestionar artículos. Utilice el módulo de "contenido" y "Galería"'
    ],
    'template_types' => [
        'rubric' => ['ru' => 'Шаблон рубрики', 'en' => 'Rubric template', 'es' => 'Plantilla de rúbrica'],
        'article' => ['ru' => 'Шаблон статьи', 'en' => 'Article template', 'es' => 'Plantilla de artículo'],
        'block' => ['ru' => 'Шаблон блока статьи', 'en' => 'Template article block', 'es' => 'Bloque de artículo de plantilla'],
    ],
    'version' => [
        'ver' => 2.1,
        'date' => '23.01.2017'
    ]
];