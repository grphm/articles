<?php
return [
    'package' => 'solutions_articles',
    'title' => ['ru' => 'Статьи', 'en' => 'Articles', 'es' => 'Artículos'],
    'route' => '__#',
    'icon' => 'zmdi zmdi-receipt',
    'menu_child' => [
        'articles' => [
            'title' => ['ru' => 'Список', 'en' => 'List', 'es' => 'Lista'],
            'route' => 'solutions.articles.index',
            'icon' => 'zmdi zmdi-view-list'
        ],
        'rubrics' => [
            'title' => ['ru' => 'Рубрики', 'en' => 'Rubrics', 'es' => 'Rúbricas'],
            'route' => 'solutions.articles.rubrics.index',
            'icon' => 'zmdi zmdi-labels'
        ],
        'templates' => [
            'title' => ['ru' => 'Шаблоны', 'en' => 'Templates', 'es' => 'Epígrafe'],
            'route' => 'solutions.articles.templates.index',
            'icon' => 'zmdi zmdi-layers'
        ]
    ]
];