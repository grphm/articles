<?php
return [
    'articles' => [
        'title' => ['ru' => 'Статьи', 'en' => 'Articles', 'es' => 'Artículos'],
        'options' => [
            ['group_title' => ['ru' => 'Основные', 'en' => 'Main', 'es' => 'Los principales']],
            'first_segment' => [
                'title' => [
                    'ru' => 'Первый сегмент в URL-адресе',
                    'en' => 'The first segment in the URL-address',
                    'es' => 'El primer segmento en la dirección de URL'
                ],
                'note' => ['ru' => '', 'en' => '', 'es' => ''],
                'type' => 'text',
                'value' => 'article'
            ],
            'rubric_segment' => [
                'title' => [
                    'ru' => 'Первый сегмент в URL-адресе рубрики',
                    'en' => 'The first segment in the URL-address of rubric',
                    'es' => 'El primer segmento en la dirección URL de la rúbrica'
                ],
                'note' => ['ru' => '', 'en' => '', 'es' => ''],
                'type' => 'text',
                'value' => 'articles-rubric'
            ],
            'rubric_articles_per_page' => [
                'title' => [
                    'ru' => 'Количество статей на странице рубрики',
                    'en' => 'Number of articles per page rubric',
                    'es' => 'Número de artículos por página rúbrica'
                ],
                'note' => [
                    'ru' => 'Оставьте пустым если нужно вывести все статьи',
                    'en' => 'Leave blank if you want to display all the articles',
                    'es' => 'Dejar en blanco si desea mostrar todos los artículos'
                ],
                'type' => 'text',
                'value' => ''
            ]
        ]
    ]
];