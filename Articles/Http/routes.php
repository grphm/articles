<?php
\Route::group(['prefix' => 'admin', 'middleware' => 'secure'], function() {

    \Route::resource('articles', 'ArticlesController',
        [
            'except' => ['show'],
            'names' => [
                'index' => 'solutions.articles.index',
                'create' => 'solutions.articles.create',
                'store' => 'solutions.articles.store',
                'edit' => 'solutions.articles.edit',
                'update' => 'solutions.articles.update',
                'destroy' => 'solutions.articles.destroy'
            ]
        ]
    );
    \Route::resource('articles/rubrics', 'RubricsController',
        [
            'except' => ['show'],
            'names' => [
                'index' => 'solutions.articles.rubrics.index',
                'create' => 'solutions.articles.rubrics.create',
                'store' => 'solutions.articles.rubrics.store',
                'edit' => 'solutions.articles.rubrics.edit',
                'update' => 'solutions.articles.rubrics.update',
                'destroy' => 'solutions.articles.rubrics.destroy'
            ]
        ]
    );
    \Route::resource('articles/templates', 'TemplatesController',
        [
            'except' => ['show'],
            'names' => [
                'index' => 'solutions.articles.templates.index',
                'create' => 'solutions.articles.templates.create',
                'store' => 'solutions.articles.templates.store',
                'edit' => 'solutions.articles.templates.edit',
                'update' => 'solutions.articles.templates.update',
                'destroy' => 'solutions.articles.templates.destroy'
            ]
        ]
    );
    \Route::post('articles/rubrics/sortable', ['as' => 'solutions.articles.rubrics.sortable', 'uses' => 'RubricsController@sortable']);
    \Route::get('articles/{article_id}/edit/blocks', ['as' => 'solutions.articles.blocks_edit', 'uses' => 'ArticleBlocksController@edit']);
    \Route::put('articles/{article_id}/edit/blocks', ['as' => 'solutions.articles.blocks_update', 'uses' => 'ArticleBlocksController@update']);
});
\Route::group(['prefix' => \PublicPage::localePrefix(), 'middleware' => 'public'], function() {

    $article_segment = settings(['solutions_articles', 'articles', 'first_segment']);
    $article_segment = empty($article_segment) ? 'article' : $article_segment;
    Route::get($article_segment.'/{article_url}', ['as' => 'public.articles.show', function($article_url) {

        return \PublicArticle::showPage($article_url);
    }]);
    $rubric_segment = settings(['solutions_articles', 'articles', 'rubric_segment']);
    $rubric_segment = empty($rubric_segment) ? 'articles-rubric' : $rubric_segment;
    Route::get($rubric_segment.'/{article_rubric_url}', ['as' => 'solutions.articles.rubrics.show', function($article_rubric_url) {

        return \PublicArticle::showRubricPage($article_rubric_url);
    }]);
});
