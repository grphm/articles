<?php
namespace STALKER_CMS\Solutions\Articles\Http\Controllers;

use Illuminate\Support\Facades\View;
use STALKER_CMS\Solutions\Articles\Models\Template;
use STALKER_CMS\Vendor\Interfaces\CrudInterface;

/**
 * Контроллер Шаблон стантей
 * Class TemplatesController
 * @package STALKER_CMS\Solutions\Articles\Http\Controllers
 */
class TemplatesController extends ModuleController implements CrudInterface {

    protected $model;
    protected $locale_prefix;

    /**
     * TemplatesController constructor.
     * @param Template $template
     */
    public function __construct(Template $template) {

        $this->model = $template;
        $this->locale_prefix = (\App::getLocale() == settings(['core_system', 'settings', 'base_locale'])) ? '' : \App::getLocale().'/';
        $this->middleware('auth');
        \PermissionsController::allowPermission('solutions_articles', 'templates');
    }

    /**
     * Список доступных шаблонов
     * @return View
     */
    public function index() {

        $templates = [];
        foreach(config('solutions_articles::config.template_types') as $type_slug => $type):
            $templates[$type_slug] = [
                'title' => array_translate($type),
                'files' => $this->model->whereLocale(\App::getLocale())->whereTemplateType($type_slug)->orderBy('updated_at', 'DESC')->get()
            ];
        endforeach;
        if(\Request::has('type') && isset($templates[\Request::get('type')])):
            $templates_type = array_first($templates, function($key, $value) {

                return $key >= \Request::get('type');
            });
            $templates = [
                \Request::get('type') => $templates_type
            ];
        endif;
        return view('solutions_articles_views::templates.index', compact('templates'));
    }

    /**
     * Создание нового шаблона
     * @return View
     */
    public function create() {

        $template_types = [];
        foreach(config('solutions_articles::config.template_types') as $type_slug => $type):
            $template_types[$type_slug] = array_translate($type);
        endforeach;
        $template_content = '';
        if(view()->exists('solutions_articles_views::templates.sketch')):
            $template_content = \File::get(view('solutions_articles_views::templates.sketch')->getPath());
        endif;
        return view('solutions_articles_views::templates.create', compact('template_content', 'template_types'));
    }

    /**
     * Сохранение нового шаблона
     * @return \Illuminate\Http\JsonResponse
     */
    public function store() {

        $request = \RequestController::isAJAX()->trim_spaces()->get();
        if(\ValidatorController::passes($request, $this->model->getStoreRules())):
            $template_directory = double_slash(base_path('home/Resources/Views/'.$this->locale_prefix.'solutions/articles/'));
            if(\File::exists($template_directory) === FALSE):
                \File::makeDirectory($template_directory, 0754, TRUE);
            endif;
            $view_path = double_slash($template_directory.$request::input('path').'.blade.php');
            if(\File::exists($view_path) === FALSE):
                $request::merge(['path' => $request::input('path').'.blade.php', 'required' => FALSE, 'locale' => \App::getLocale()]);
                $this->model->insert($request);
                \File::put($view_path, $request::input('content'));
                return \ResponseController::success(1600)->redirect(route('solutions.articles.templates.index'))->json();
            else:
                return \ResponseController::error(2610)->json();
            endif;
        else:
            return \ResponseController::error(2100)->json();
        endif;
    }

    /**
     * Редактирование шаблона
     * @param $id
     * @return View
     */
    public function edit($id) {

        $template_content = '';
        $template = $this->model->findOrFail($id);
        $view_path = base_path('/home/Resources/Views/'.$this->locale_prefix.'solutions/articles/'.$template->path);
        if(\File::exists(realpath($view_path))):
            $template_content = \File::get(realpath($view_path));
        endif;
        return view('solutions_articles_views::templates.edit', compact('template_content', 'template'));
    }

    /**
     * Обновление шаблона
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function update($id) {

        $request = \RequestController::isAJAX()->trim_spaces()->get();
        if(\ValidatorController::passes($request, $this->model->getUpdateRules())):
            $template = $this->model->findOrFail($id);
            $view_path = double_slash(base_path('home/Resources/Views/'.$this->locale_prefix.'solutions/articles/').$template->path);
            \File::put($view_path, $request::input('content'));
            $this->model->replace($id, $request);
            return \ResponseController::success(202)->redirect(route('solutions.articles.templates.index'))->json();
        else:
            return \ResponseController::error(2100)->json();
        endif;
    }

    /**
     * Удаление шаблона
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy($id) {

        \RequestController::isAJAX()->init();
        try {
            $template = $this->model->findOrFail($id);
            $view_path = base_path('/home/Resources/Views/'.$this->locale_prefix.'solutions/articles/'.$template->path);
            if(\File::exists(realpath($view_path))):
                \File::delete(realpath($view_path));
            endif;
            $this->model->remove($id);
            return \ResponseController::success(1203)->redirect(route('solutions.articles.templates.index'))->json();
        } catch(\Exception $e) {
            return \ResponseController::success(2503)->json();
        }
    }
}