<?php
namespace STALKER_CMS\Solutions\Articles\Http\Controllers;

use STALKER_CMS\Solutions\Articles\Models\Article;
use STALKER_CMS\Solutions\Articles\Models\ArticleBlock;
use STALKER_CMS\Solutions\Articles\Models\Rubric;
use STALKER_CMS\Solutions\Articles\Models\Template;
use STALKER_CMS\Vendor\Interfaces\CrudInterface;

class ArticlesController extends ModuleController implements CrudInterface {

    protected $model;
    protected $template;

    public function __construct(Article $article, Template $template) {

        $this->model = $article;
        $this->template = $template;
        $this->middleware('auth');
    }

    /**
     * Список статей
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index() {

        \PermissionsController::allowPermission('solutions_articles', 'articles');
        $request = \RequestController::init();
        $articles = $this->model->whereLocale(\App::getLocale());
        if($request::has('sort_field') && $request::has('sort_direction')):
            foreach(explode(', ', $request::get('sort_field')) as $index):
                $articles = $articles->orderBy($index, $request::get('sort_direction'));
            endforeach;
        endif;
        $rubric = NULL;
        if(\Request::has('rubric')):
            if($rubric = Rubric::whereLocale(\App::getLocale())->whereSlug(\Request::input('rubric'))->orderBy('order')->first()):
                $articles = $articles->whereRubricId($rubric->id);
            endif;
        endif;
        if($request::has('search')):
            $search = $request::get('search');
            $articles = $articles->where(function($query) use ($search) {

                $query->where('title', 'like', '%'.$search.'%');
            });
        endif;
        $articles = $articles->orderBy('created_at', 'DESC')->with('rubric', 'author')->paginate(20);
        return view('solutions_articles_views::articles.index', compact('articles', 'rubric'));
    }

    /**
     * Создание статьи
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create() {

        \PermissionsController::allowPermission('solutions_articles', 'create');
        $templates = $this->template->whereLocale(\App::getLocale())->whereTemplateType('article')->lists('title', 'id');
        if($templates->count()):
            $blocks = $this->template->whereLocale(\App::getLocale())->whereTemplateType('block')->lists('title', 'id');
            return view('solutions_articles_views::articles.create', compact('templates', 'blocks'));
        else:
            return redirect()->route('solutions.articles.templates.create')->with('status', 2616);
        endif;
    }

    /**
     * Сохранение статьи
     * @return \Illuminate\Http\JsonResponse
     */
    public function store() {

        \PermissionsController::allowPermission('solutions_articles', 'create');
        $request = \RequestController::isAJAX()->init();
        if(\ValidatorController::passes($request, $this->model->getStoreRules())):
            if(\PermissionsController::isPackageEnabled('core_seo')):
                $this->model->forbiddenURL($request::input('seo_url'));
                $this->model->uniqueSeoURL($request::input('seo_url'));
            endif;
            if(\PermissionsController::isPackageEnabled('core_open_graph')):
                $open_graph = \OpenGraph::makeOpenGraphData($request);
                $request::merge(['open_graph' => $open_graph]);
            endif;
            if($image = $this->uploadImages($request)):
                $request::merge($image);
            endif;
            $article = $this->model->insert($request);
            $structure = [];
            foreach($request::input('content') as $template_block_id):
                $template = $this->template->findOrFail($template_block_id);
                $view_path = base_path('/home/Resources/Views/'.$this->locale_prefix.'solutions/articles/'.$template->path);
                if(\File::exists(realpath($view_path))):
                    $template_content = \File::get(realpath($view_path));
                    $request::merge(['article_id' => $article->id, 'template_id' => $template_block_id, 'content' => $template_content]);
                    $block = new ArticleBlock();
                    $articleBlock = $block->insert($request);
                    $structure[$articleBlock->id] = $template->id;
                endif;
            endforeach;
            $article->structure = json_encode($structure);
            $article->save();
            return \ResponseController::success(201)->redirect(route('solutions.articles.index'))->json();
        else:
            return \ResponseController::error(2100)->json();
        endif;
    }

    /**
     * Редактирование статьи
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit($id) {

        \PermissionsController::allowPermission('solutions_articles', 'edit');
        $article = $this->model->findOrFail($id);
        $blocks = [];
        $template_blocks = $this->template->whereLocale(\App::getLocale())->whereTemplateType('block')->lists('title', 'id');
        foreach(json_decode($article->structure, TRUE) as $block_id => $template_id):
            $blocks[$block_id] = $template_id;
        endforeach;
        $templates = $this->template->whereLocale(\App::getLocale())->whereTemplateType('article')->lists('title', 'id');
        return view('solutions_articles_views::articles.edit', compact('article', 'templates', 'template_blocks', 'blocks'));
    }

    /**
     * Обновление статьи
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function update($id) {

        \PermissionsController::allowPermission('solutions_articles', 'edit');
        $request = \RequestController::isAJAX()->init();
        if(\ValidatorController::passes($request, $this->model->getUpdateRules())):
            $article = $this->model->findOrFail($id);
            if(\PermissionsController::isPackageEnabled('core_seo')):
                $this->model->forbiddenURL($request::input('seo_url'));
                $this->model->uniqueSeoURL($request::input('seo_url'), $id);
            endif;
            if(\PermissionsController::isPackageEnabled('core_open_graph')):
                $open_graph = \OpenGraph::remakeOpenGraphData($article, $request);
                $request::merge(['open_graph' => $open_graph]);
            endif;
            if($images = $this->uploadImages($request, $article)):
                $request::merge($images);
            else:
                $request::merge(['main_image' => $article->main_image, 'announce_image' => $article->announce_image]);
            endif;
            $this->model->replace($id, $request);
            $structure = json_decode($article->structure, TRUE);
            /****************** Удаление текстового блока ********/
            foreach($article->blocks_structure as $old_structure_item):
                if(!in_array($old_structure_item, $request::input('content'))):
                    if($block_id = explode('|', $old_structure_item)[0]):
                        ArticleBlock::whereId($block_id)->delete();
                        unset($structure[$block_id]);
                    endif;
                endif;
            endforeach;
            /****************** Добавление текстового блока ******/
            foreach($request::input('content') as $new_structure_item):
                if(!in_array($new_structure_item, $article->blocks_structure)):
                    $template = $this->template->findOrFail($new_structure_item);
                    $view_path = base_path('/home/Resources/Views/'.$this->locale_prefix.'solutions/articles/'.$template->path);
                    if(\File::exists(realpath($view_path))):
                        $template_content = \File::get(realpath($view_path));
                        $request::merge(['article_id' => $article->id, 'template_id' => $new_structure_item, 'content' => $template_content]);
                        $block = new ArticleBlock();
                        $articleBlock = $block->insert($request);
                        $structure[$articleBlock->id] = $template->id;
                    endif;
                endif;
            endforeach;
            $article->structure = json_encode($structure);
            $article->save();
            return \ResponseController::success(202)->redirect(route('solutions.articles.index'))->json();
        else:
            return \ResponseController::error(2100)->json();
        endif;
    }

    /**
     * Удаление статьи
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy($id) {

        \PermissionsController::allowPermission('solutions_articles', 'delete');
        \RequestController::isAJAX()->init();
        $article = $this->model->findOrFail($id);
        if(\PermissionsController::isPackageEnabled('core_open_graph')):
            \OpenGraph::destroyOpenGraphData($article);
        endif;
        $this->deleteImages($article);
        $this->model->remove($id);
        return \ResponseController::success(1203)->redirect(route('solutions.articles.index'))->json();
    }

    /**************************************************************************************************************/
    private function uploadImages(\Request $request, $article = NULL) {

        if(!is_null($article)):
            $images = ['main_image' => $article->main_image, 'announce_image' => $article->announce_image];
        else:
            $images = ['main_image' => NULL, 'announce_image' => NULL];
        endif;
        if($request::input('main_image_delete') == 1):
            if(!empty($article->main_image) && \Storage::exists($article->main_image)):
                \Storage::delete($article->main_image);
                $article->main_image = NULL;
                $article->save();
            endif;
            $images['main_image'] = NULL;
        endif;
        if($request::input('announce_image_delete') == 1):
            if(!empty($article->announce_image) && \Storage::exists($article->announce_image)):
                \Storage::delete($article->announce_image);
                $article->announce_image = NULL;
                $article->save();
            endif;
            $images['announce_image'] = NULL;
        endif;
        if($request::hasFile('main_image')):
            $fileName = time()."_".rand(1000, 1999).'.'.$request::file('main_image')->getClientOriginalExtension();
            $mainPhotoPath = 'content/articles';
            $request::file('main_image')->move('uploads/'.$mainPhotoPath, $fileName);
            $images['main_image'] = add_first_slash($mainPhotoPath.'/'.$fileName);
            if(!empty($article->main_image) && \Storage::exists($article->main_image)):
                \Storage::delete($article->main_image);
                $article->main_image = NULL;
                $article->save();
            endif;
        endif;
        if($request::hasFile('announce_image')):
            $fileName = time()."_".rand(1000, 1999).'.'.$request::file('announce_image')->getClientOriginalExtension();
            $announcePhotoPath = 'content/articles/announce';
            $request::file('announce_image')->move('uploads/'.$announcePhotoPath, $fileName);
            $images['announce_image'] = add_first_slash($announcePhotoPath.'/'.$fileName);
            if(!empty($article->announce_image) && \Storage::exists($article->announce_image)):
                \Storage::delete($article->announce_image);
                $article->announce_image = NULL;
                $article->save();
            endif;
        endif;
        return $images;
    }

    private function deleteImages(Article $article) {

        if(!empty($article->main_image) && \Storage::exists($article->main_image)):
            \Storage::delete($article->main_image);
            $article->main_image = NULL;
        endif;
        if(!empty($article->announce_image) && \Storage::exists($article->announce_image)):
            \Storage::delete($article->announce_image);
            $article->announce_image = NULL;
        endif;
        $article->save();
    }
}