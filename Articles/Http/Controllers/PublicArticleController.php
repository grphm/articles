<?php
namespace STALKER_CMS\Solutions\Articles\Http\Controllers;

use STALKER_CMS\Solutions\Articles\Models\Article;
use STALKER_CMS\Solutions\Articles\Models\Rubric;
use STALKER_CMS\Solutions\Articles\Models\Template;

class PublicArticleController extends ModuleController {

    protected $model;
    protected $rubric;

    public function __construct() {

        $this->model = new Article();
        $this->rubric = new Rubric();
    }

    /**
     * Отображает страницу статьи
     * @param $url
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function showPage($url) {

        if(settings(['core_system', 'settings', 'services_mode'])):
            if(view()->exists("home_views::errors.1503")):
                return view("home_views::errors.1503", ['code' => 1503, 'message' => trans('root_lang::codes.1503')]);
            elseif(view()->exists("root_views::errors.1503")):
                return view("root_views::errors.1503", ['code' => 1503, 'message' => trans('root_lang::codes.1503')]);
            endif;
        endif;
        if(is_numeric($url)):
            $page = $this->model->whereLocale(\App::getLocale())
                ->whereId($url)->wherePublication(TRUE)
                ->with('rubric', 'template', 'blocks', 'author')
                ->first();
        else:
            $page = $this->model->whereLocale(\App::getLocale())
                ->whereSeoUrl($url)->wherePublication(TRUE)
                ->with('rubric', 'template', 'blocks', 'author')
                ->first();
        endif;
        if($page):
            $template = Template::findOrFail($page->template_id);
            $view_path = $this->getViewPath($template);
            if(view()->exists("home_views::$view_path")):
                $page->increment_views();
                return view("home_views::$view_path", compact('page'));
            endif;
        endif;
        abort(404);
    }

    /**
     * Отображает страницу рубрики
     * @param $url
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function showRubricPage($url) {

        if(settings(['core_system', 'settings', 'services_mode'])):
            if(view()->exists("home_views::errors.1503")):
                return view("home_views::errors.1503", ['code' => 1503, 'message' => trans('root_lang::codes.1503')]);
            elseif(view()->exists("root_views::errors.1503")):
                return view("root_views::errors.1503", ['code' => 1503, 'message' => trans('root_lang::codes.1503')]);
            endif;
        endif;
        if(is_numeric($url)):
            $page = $this->rubric->whereLocale(\App::getLocale())->whereId($url)->first();
        else:
            $page = $this->rubric->whereLocale(\App::getLocale())
                ->where(function($query) use ($url) {

                    $query->where('slug', $url);
                    $query->orWhere('seo_url', $url);
                })
                ->first();
        endif;
        if($page):
            $template = Template::findOrFail($page->template_id);
            $view_path = $this->getViewPath($template);
            if(view()->exists("home_views::$view_path")):
                $articles = $this->model->whereLocale(\App::getLocale())->wherePublication(TRUE)->where('rubric_id', $page->id);
                if(\Request::has('date')):
                    $articles = $articles->where('published_at', \Request::input('date'));
                endif;
                if($perPage = settings(['solutions_articles', 'articles', 'rubric_articles_per_page'])):
                    $articles = $articles->paginate($perPage);
                else:
                    $articles = $articles->get();
                endif;
                return view("home_views::$view_path", compact('page', 'articles'));
            endif;
        endif;
        abort(404);
    }

    /**
     * Возвращает список тегов статьи
     * Если передать ID статьи вернет список тегов заданной статьи
     * @param null $article_id
     * @return array
     */
    public function getTags($article_id = NULL) {

        $articleTags = [];
        $articles = $this->model->whereLocale(\App::getLocale())->wherePublication(TRUE);
        if(is_null($article_id)):
            foreach($articles = $articles->where('tags', '!=', '')->whereNotNull('tags')->lists('tags') as $articles_tags):
                foreach(explode(',', $articles_tags) as $tag):
                    if(!isset($articleTags[$tag])):
                        $articleTags[$tag] = 1;
                    else:
                        $articleTags[$tag]++;
                    endif;
                endforeach;
            endforeach;
        else:
            if($article = $articles->whereId($article_id)->first()):
                foreach(explode(',', $article->tags) as $tag):
                    if(!isset($articleTags[$tag])):
                        $articleTags[$tag] = 1;
                    else:
                        $articleTags[$tag]++;
                    endif;
                endforeach;
            endif;
        endif;
        return $articleTags;
    }

    /**
     * Возвращает список статей
     * @param null $limit
     * @param bool $only_top
     * @return mixed
     */
    public function getArticles($limit = NULL, $only_top = FALSE) {

        $articles = $this->model->whereLocale(\App::getLocale())
            ->wherePublication(TRUE)
            ->orderBy('published_at', 'DESC')
            ->with('rubric', 'template', 'author');
        if($only_top):
            $articles = $articles->whereTop(TRUE);
        endif;
        if(!is_null($limit)):
            return $articles->paginate($limit);
        else:
            return $articles->get();
        endif;
    }

    /**
     * Возвращает статьи по заданному тегу
     * @param null $tag
     * @param null $limit
     * @return mixed
     */
    public function getTagArticles($tag = NULL, $limit = NULL) {

        $articles = $this->model->whereLocale(\App::getLocale())
            ->where('tags', 'like', '%'.$tag.'%')
            ->wherePublication(TRUE)
            ->orderBy('published_at', 'DESC');
        if(!is_null($limit)):
            return $articles->paginate($limit);
        else:
            return $articles->get();
        endif;
    }

    /**
     * Возвращает список тегов рубрики
     * Если передать ID рубрики вернет список тегов заданной рубрики
     * @param null $rubric_id
     * @return array
     */
    public function getRubricTags($rubric_id = NULL) {

        $rubricTags = [];
        if(is_null($rubric_id)):
            foreach($this->rubric->where('tags', '!=', '')->whereNotNull('tags')->lists('tags') as $rubric_tags):
                foreach(explode(',', $rubric_tags) as $tag):
                    if(!isset($rubricTags[$tag])):
                        $rubricTags[$tag] = 1;
                    else:
                        $rubricTags[$tag]++;
                    endif;
                endforeach;
            endforeach;
        else:
            if($rubric = $this->rubric->whereLocale(\App::getLocale())->whereId($rubric_id)->first()):
                foreach(explode(',', $rubric->tags) as $tag):
                    if(!isset($rubricTags[$tag])):
                        $rubricTags[$tag] = 1;
                    else:
                        $rubricTags[$tag]++;
                    endif;
                endforeach;
            endif;
        endif;
        return $rubricTags;
    }

    /**
     * Возвращает список доступных рубрик для текущей локали
     * @return mixed
     */
    public function getRubricsList() {

        $rubrics = [];
        $rubrics[] = \Lang::get('solutions_articles_lang::articles.rubric_empty');
        foreach($this->rubric->whereLocale(\App::getLocale())->orderBy('order')->lists('title', 'id') as $id => $title):
            $rubrics[$id] = $title;
        endforeach;
        return collect($rubrics);
    }

    /**************************************************************************************************************/
    /**
     * Вернуть путь в файлу шаблона рубрики
     * @param $template
     * @return string
     */
    private function getViewPath($template) {

        $locale = $this->getLocalePrefix($template->locale);
        $view_path = remove_first_slash($locale.'solutions.articles.'.$template->path);
        return substr($view_path, 0, -10);
    }

    /**
     * Изменить локаль и вернуть ссылку на страницу рубрики
     * @param $locale
     * @return \Illuminate\Contracts\Routing\UrlGenerator|mixed|string
     */
    public function makeLocaleURL($locale, $page) {

        $locale = $this->getLocalePrefix($locale);
        if($page instanceof Article):
            $segment = settings(['solutions_articles', 'articles', 'first_segment']);
            $segment = empty($segment) ? 'article' : $segment;
        elseif($page instanceof Rubric):
            $segment = settings(['solutions_articles', 'articles', 'rubric_segment']);
            $segment = empty($segment) ? 'articles-rubric' : $segment;
        endif;
        return url($locale.$segment.'/'.$page->pageUrl);
    }

    /**
     * Возвращает префикс локали
     * @param $locale
     * @return string
     */
    private function getLocalePrefix($locale) {

        if($locale == settings(['core_system', 'settings', 'base_locale'])):
            $locale = '';
        else:
            $locale .= '/';
        endif;
        return $locale;
    }
}