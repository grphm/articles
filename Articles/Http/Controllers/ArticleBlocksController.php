<?php
namespace STALKER_CMS\Solutions\Articles\Http\Controllers;

use STALKER_CMS\Solutions\Articles\Models\Article;
use STALKER_CMS\Solutions\Articles\Models\ArticleBlock;

class ArticleBlocksController extends ModuleController {

    protected $model;
    protected $article;

    public function __construct(ArticleBlock $articleBlock, Article $article) {

        $this->model = $articleBlock;
        $this->article = $article;
        $this->middleware('auth');
        \PermissionsController::allowPermission('solutions_articles', 'edit');
    }

    /**
     * Редактирование контентных блоков статьи
     * @param $article_id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit($article_id) {

        $article = $this->article->findOrFail($article_id);
        $blocks = $this->model->whereArticleId($article_id)->orderBy('id')->with('template')->get();
        return view('solutions_articles_views::blocks.edit', compact('article', 'blocks'));
    }

    /**
     * Обновление контентных блоков статьи
     * @param $article_id
     * @return \Illuminate\Http\JsonResponse
     */
    public function update($article_id) {

        $request = \RequestController::isAJAX()->trim_spaces()->get();
        if(\ValidatorController::passes($request, $this->model->getUpdateRules())):
            $article = $this->article->findOrFail($article_id);
            foreach($request::input('blocks') as $index => $block_id):
                ArticleBlock::whereId($block_id)->whereArticleId($article->id)->update(['content' => $request::input('contents')[$index]]);
            endforeach;
            return \ResponseController::success(202)->redirect(route('solutions.articles.index'))->json();
        else:
            return \ResponseController::error(2100)->json();
        endif;
    }
}