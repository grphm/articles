<?php
namespace STALKER_CMS\Solutions\Articles\Http\Controllers;

use STALKER_CMS\Solutions\Articles\Models\Rubric;
use STALKER_CMS\Solutions\Articles\Models\Template;
use STALKER_CMS\Vendor\Interfaces\CrudInterface;

class RubricsController extends ModuleController implements CrudInterface {

    protected $model;
    protected $template;

    public function __construct(Rubric $rubric, Template $template) {

        $this->model = $rubric;
        $this->template = $template;
        $this->middleware('auth');
        \PermissionsController::allowPermission('solutions_articles', 'rubrics');
    }

    /**
     * Список доступных рубрик
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index() {

        $request = \RequestController::init();
        $rubrics = $this->model->whereLocale(\App::getLocale());
        if($request::has('sort_field') && $request::has('sort_direction')):
            foreach(explode(', ', $request::get('sort_field')) as $index):
                $rubrics = $rubrics->orderBy($index, $request::get('sort_direction'));
            endforeach;
        endif;
        if($request::has('search')):
            $search = $request::get('search');
            $rubrics = $rubrics->where(function($query) use ($search) {

                $query->where('title', 'like', '%'.$search.'%');
            });
        endif;
        $rubrics = $rubrics->orderBy('order')->with('template')->get();
        return view('solutions_articles_views::rubrics.index', compact('rubrics'));
    }

    /**
     * Создание рубрики
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create() {

        $templates = $this->template->whereLocale(\App::getLocale())->whereTemplateType('rubric')->lists('title', 'id');
        if($templates->count()):
            return view('solutions_articles_views::rubrics.create', compact('templates'));
        else:
            return redirect()->route('solutions.articles.templates.create')->with('status', 2616);
        endif;
    }

    public function store() {

        $request = \RequestController::isAJAX()->init();
        if(\ValidatorController::passes($request, $this->model->getStoreRules())):
            $this->model->uniqueness($request::only('slug'));
            if(\PermissionsController::isPackageEnabled('core_seo')):
                $this->model->forbiddenURL($request::input('seo_url'));
                $this->model->uniqueSeoURL($request::input('seo_url'));
            endif;
            if(\PermissionsController::isPackageEnabled('core_open_graph')):
                $open_graph = \OpenGraph::makeOpenGraphData($request);
                $request::merge(['open_graph' => $open_graph]);
            endif;
            if($image = $this->uploadImages($request)):
                $request::merge($image);
            endif;
            $this->model->insert($request);
            return \ResponseController::success(201)->redirect(route('solutions.articles.rubrics.index'))->json();
        else:
            return \ResponseController::error(2100)->json();
        endif;
    }

    public function edit($id) {

        $rubric = $this->model->findOrFail($id);
        $templates = $this->template->whereLocale(\App::getLocale())->lists('title', 'id');
        return view('solutions_articles_views::rubrics.edit', compact('rubric', 'templates'));
    }

    public function update($id) {

        $request = \RequestController::isAJAX()->init();
        if(\ValidatorController::passes($request, $this->model->getUpdateRules())):
            $rubric = $this->model->findOrFail($id);
            $this->model->uniqueness($request::only('slug'), $id);
            if(\PermissionsController::isPackageEnabled('core_seo')):
                $this->model->forbiddenURL($request::input('seo_url'));
                $this->model->uniqueSeoURL($request::input('seo_url'), $id);
            endif;
            if(\PermissionsController::isPackageEnabled('core_open_graph')):
                $open_graph = \OpenGraph::remakeOpenGraphData($rubric, $request);
                $request::merge(['open_graph' => $open_graph]);
            endif;
            if($images = $this->uploadImages($request, $rubric)):
                $request::merge($images);
            else:
                $request::merge(['main_image' => $rubric->main_image, 'announce_image' => $rubric->announce_image]);
            endif;
            $this->model->replace($id, $request);
            return \ResponseController::success(202)->redirect(route('solutions.articles.rubrics.index'))->json();
        else:
            return \ResponseController::error(2100)->json();
        endif;
    }

    public function destroy($id) {

        \RequestController::isAJAX()->init();
        $rubric = $this->model->findOrFail($id);
        if(\PermissionsController::isPackageEnabled('core_open_graph')):
            \OpenGraph::destroyOpenGraphData($rubric);
        endif;
        $this->deleteImages($rubric);
        $this->model->remove($id);
        return \ResponseController::success(1203)->redirect(route('solutions.articles.rubrics.index'))->json();
    }

    public function sortable(){

        $request = \RequestController::isAJAX()->init();
        if(\ValidatorController::passes($request, ['elements' => 'required'])):
            foreach(explode(',', $request::input('elements')) as $index => $slide_id):
                $this->model->where('id', $slide_id)->update(['order' => $index + 1]);
            endforeach;
            return \ResponseController::success(202)->json();
        else:
            return \ResponseController::error(2100)->json();
        endif;
    }

    /**************************************************************************************************************/
    private function uploadImages(\Request $request, $rubric = NULL) {

        if(!is_null($rubric)):
            $images = ['main_image' => $rubric->main_image, 'announce_image' => $rubric->announce_image];
        else:
            $images = ['main_image' => NULL, 'announce_image' => NULL];
        endif;
        if($request::hasFile('main_image')):
            $fileName = time()."_".rand(1000, 1999).'.'.$request::file('main_image')->getClientOriginalExtension();
            $mainPhotoPath = 'content/articles/rubrics';
            $request::file('main_image')->move('uploads/'.$mainPhotoPath, $fileName);
            $images['main_image'] = add_first_slash($mainPhotoPath.'/'.$fileName);
            if(!empty($rubric->main_image) && \Storage::exists($rubric->main_image)):
                \Storage::delete($rubric->main_image);
                $rubric->main_image = NULL;
                $rubric->save();
            endif;
        endif;
        if($request::hasFile('announce_image')):
            $fileName = time()."_".rand(1000, 1999).'.'.$request::file('announce_image')->getClientOriginalExtension();
            $announcePhotoPath = 'content/articles/rubrics';
            $request::file('announce_image')->move('uploads/'.$announcePhotoPath, $fileName);
            $images['announce_image'] = add_first_slash($announcePhotoPath.'/'.$fileName);
            if(!empty($rubric->announce_image) && \Storage::exists($rubric->announce_image)):
                \Storage::delete($rubric->announce_image);
                $rubric->announce_image = NULL;
                $rubric->save();
            endif;
        endif;
        return $images;
    }

    private function deleteImages(Rubric $rubric) {

        if(!empty($rubric->main_image) && \Storage::exists($rubric->main_image)):
            \Storage::delete($rubric->main_image);
            $rubric->main_image = NULL;
        endif;
        if(!empty($rubric->announce_image) && \Storage::exists($rubric->announce_image)):
            \Storage::delete($rubric->announce_image);
            $rubric->announce_image = NULL;
        endif;
        $rubric->save();
    }
}