<?php
namespace STALKER_CMS\Solutions\Articles\Facades;

use Illuminate\Support\Facades\Facade;

class PublicArticle extends Facade {

    protected static function getFacadeAccessor() {

        return 'PublicArticleController';
    }
}