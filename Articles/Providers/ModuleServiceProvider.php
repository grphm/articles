<?php
namespace STALKER_CMS\Solutions\Articles\Providers;

use STALKER_CMS\Solutions\Articles\Http\Controllers\PublicArticleController;
use STALKER_CMS\Vendor\Providers\ServiceProvider as BaseServiceProvider;

class ModuleServiceProvider extends BaseServiceProvider {

    public function boot() {

        $this->setPath(__DIR__.'/../');
        $this->registerViews('solutions_articles_views');
        $this->registerLocalization('solutions_articles_lang');
        $this->registerConfig('solutions_articles::config', 'Config/articles.php');
        $this->registerSettings('solutions_articles::settings', 'Config/settings.php');
        $this->registerActions('solutions_articles::actions', 'Config/actions.php');
        $this->registerSystemMenu('solutions_articles::menu', 'Config/menu.php');
        $this->publishesTemplates();
    }

    public function register() {

        \App::bind('PublicArticleController', function() {

            return new PublicArticleController();
        });
    }

    public function publishesTemplates() {

        $this->publishes([
            __DIR__.'/../Resources/Templates' => base_path('home/Resources')
        ]);
    }
}
