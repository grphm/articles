<?php
namespace STALKER_CMS\Solutions\Articles\Models;

use Carbon\Carbon;
use STALKER_CMS\Vendor\Interfaces\ModelInterface;
use STALKER_CMS\Vendor\Models\BaseModel;
use STALKER_CMS\Vendor\Traits\ModelTrait;
use STALKER_CMS\Core\OpenGraph\Traits\OpenGraphTrait;
use STALKER_CMS\Core\Seo\Traits\SeoTrait;

class Article extends BaseModel implements ModelInterface {

    use ModelTrait, OpenGraphTrait, SeoTrait;
    protected $table = 'solution_articles';
    protected $fillable = ['locale', 'rubric_id', 'template_id', 'published_at', 'publication', 'title', 'announce', 'structure', 'tags', 'top', 'user_id'];
    protected $hidden = [];
    protected $guarded = [];
    protected $dates = ['published_at', 'created_at', 'updated_at'];

    public function insert($request) {

        $this->locale = \App::getLocale();
        $this->rubric_id = $request::input('rubric_id');
        $this->template_id = $request::input('template_id');
        $this->published_at = $request::has('published_at') ? Carbon::parse($request::get('published_at'))->format('Y-m-d 00:00:00') : NULL;
        $this->publication = $request::has('publication') ? TRUE : FALSE;
        $this->title = $request::input('title');
        $this->announce = $request::input('announce');
        $this->structure = $request::input('structure');
        $this->main_image = $request::has('main_image') ? $request::input('main_image') : NULL;
        $this->announce_image = $request::has('announce_image') ? $request::input('announce_image') : NULL;
        $this->tags = $request::input('tags');
        $this->seo_title = $request::has('seo_title') && $request::input('seo_title') != '' ? $request::input('seo_title') : NULL;
        $this->seo_keywords = $request::has('seo_keywords') && $request::input('seo_keywords') != '' ? $request::input('seo_keywords') : NULL;
        $this->seo_description = $request::has('seo_description') && $request::input('seo_description') != '' ? $request::input('seo_description') : NULL;
        $this->seo_h1 = $request::has('seo_h1') && $request::input('seo_h1') != '' ? $request::input('seo_h1') : NULL;
        $this->seo_url = $request::has('seo_url') && $request::input('seo_url') != '' ? $request::input('seo_url') : NULL;
        $this->open_graph = $request::has('open_graph') ? $request::input('open_graph') : NULL;
        $this->views = 0;
        $this->top = $request::has('top') ? TRUE : FALSE;
        $this->user_id = \Auth::id();
        $this->created_at = Carbon::now();
        $this->updated_at = Carbon::now();
        $this->save();
        return $this;
    }

    public function replace($id, $request) {

        $model = $this::whereFind(['id' => $id]);
        $model->rubric_id = $request::input('rubric_id');
        $model->template_id = $request::input('template_id');
        $model->published_at = $request::has('published_at') ? Carbon::parse($request::get('published_at'))->format('Y-m-d 00:00:00') : NULL;
        $model->publication = $request::has('publication') ? TRUE : FALSE;
        $model->title = $request::input('title');
        $model->announce = $request::input('announce');
        $model->main_image = $request::has('main_image') ? $request::input('main_image') : NULL;
        $model->announce_image = $request::has('announce_image') ? $request::input('announce_image') : NULL;
        $model->tags = $request::input('tags');
        $model->seo_title = $request::has('seo_title') && $request::input('seo_title') != '' ? $request::input('seo_title') : NULL;
        $model->seo_keywords = $request::has('seo_keywords') && $request::input('seo_keywords') != '' ? $request::input('seo_keywords') : NULL;
        $model->seo_description = $request::has('seo_description') && $request::input('seo_description') != '' ? $request::input('seo_description') : NULL;
        $model->seo_h1 = $request::has('seo_h1') && $request::input('seo_h1') != '' ? $request::input('seo_h1') : NULL;
        $model->seo_url = $request::has('seo_url') && $request::input('seo_url') != '' ? $request::input('seo_url') : NULL;
        $model->open_graph = $request::has('open_graph') ? $request::input('open_graph') : NULL;
        $model->top = $request::has('top') ? TRUE : FALSE;
        $model->user_id = \Auth::id();
        $model->updated_at = Carbon::now();
        $model->save();
        return $model;
    }

    public function remove($id) {

        $instance = static::findOrFail($id);
        ArticleBlock::whereArticleId($instance->id)->delete();
        return $instance->delete();
    }

    public function search(array $attributes) {
        // TODO: Implement search() method.
    }

    public function filter(array $attributes) {
        // TODO: Implement filter() method.
    }

    public function sort(array $attributes) {
        // TODO: Implement sort() method.
    }

    public function getPublishedDateAttribute() {

        if($this->attributes['created_at']):
            Carbon::setLocale(\App::getLocale());
            return Carbon::parse($this->attributes['created_at'])->diffForHumans();
        else:
            return '<span class="c-red">'.\Lang::get('core_content_lang::pages.not_published').'</span >';
        endif;
    }

    public function getInTopAttribute() {

        if($this->attributes['top']):
            return '<span class="c-green">'.\Lang::get('solutions_articles_lang::articles.in_top.yes').'</span >';
        else:
            return \Lang::get('solutions_articles_lang::articles.in_top.no');
        endif;
    }

    public function getBlocksStructureAttribute() {

        if($this->attributes['structure']):
            $structure = [];
            foreach(json_decode($this->attributes['structure'], TRUE) as $block_id => $template_id):
                $structure[] = $block_id.'|'.$template_id;
            endforeach;
            return $structure;
        endif;
    }

    public function getAssetImageAttribute() {

        if($this->attributes['main_image']):
            return asset('uploads'.$this->attributes['main_image']);
        else:
            return NULL;
        endif;
    }

    public function getAssetAnnounceImageAttribute() {

        if($this->attributes['announce_image']):
            return asset('uploads'.$this->attributes['announce_image']);
        else:
            return NULL;
        endif;
    }

    /**
     * Рубрика
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function rubric() {

        return $this->hasOne('\STALKER_CMS\Solutions\Articles\Models\Rubric', 'id', 'rubric_id');
    }

    /**
     * Шаблон
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function template() {

        return $this->hasOne('\STALKER_CMS\Solutions\Articles\Models\Template', 'id', 'template_id');
    }

    /**
     * Контентные блоки
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function blocks() {

        return $this->hasMany('\STALKER_CMS\Solutions\Articles\Models\ArticleBlock', 'article_id', 'id');
    }

    public function author() {

        return $this->hasOne('\STALKER_CMS\Core\System\Models\User', 'id', 'user_id');
    }

    public function increment_views() {

        $this->views = $this->views + 1;
        $this->save();
        return $this;
    }

    /***************************************************************************************************************/
    public static function getStoreRules() {

        return ['rubric_id' => 'required', 'template_id' => 'required', 'published_at' => 'required', 'title' => 'required', 'content' => 'required'];
    }

    public static function getUpdateRules() {

        return ['rubric_id' => 'required', 'template_id' => 'required', 'published_at' => 'required', 'title' => 'required', 'content' => 'required'];
    }
}