<?php
namespace STALKER_CMS\Solutions\Articles\Models;

use Carbon\Carbon;
use STALKER_CMS\Vendor\Interfaces\ModelInterface;
use STALKER_CMS\Vendor\Models\BaseModel;
use STALKER_CMS\Vendor\Traits\ModelTrait;

class ArticleBlock extends BaseModel implements ModelInterface {

    use ModelTrait;
    protected $table = 'solution_articles_content_blocks';
    protected $fillable = ['article_id', 'template_id', 'content', 'user_id'];
    protected $hidden = [];
    protected $guarded = [];

    public function insert($request) {

        $this->article_id = $request::input('article_id');
        $this->template_id = $request::input('template_id');
        $this->content = $request::input('content');
        $this->user_id = \Auth::id();
        $this->created_at = Carbon::now();
        $this->updated_at = Carbon::now();
        $this->save();
        return $this;
    }

    public function replace($id, $request) {

        $model = $this::whereFind(['id' => $id]);
        $model->content = $request::input('content');
        $model->user_id = \Auth::id();
        $model->updated_at = Carbon::now();
        $model->save();
        return $model;
    }

    public function remove($id) {

        $instance = static::findOrFail($id);
        return $instance->delete();
    }

    public function search(array $attributes) {
        // TODO: Implement search() method.
    }

    public function filter(array $attributes) {
        // TODO: Implement filter() method.
    }

    public function sort(array $attributes) {
        // TODO: Implement sort() method.
    }

    public function author() {

        return $this->hasOne('\STALKER_CMS\Core\System\Models\User', 'id', 'user_id');
    }

    /**
     * Шаблон
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function template() {

        return $this->hasOne('\STALKER_CMS\Solutions\Articles\Models\Template', 'id', 'template_id');
    }

    /***************************************************************************************************************/
    public static function getStoreRules() {

        return [];
    }

    public static function getUpdateRules() {

        return [];
    }
}