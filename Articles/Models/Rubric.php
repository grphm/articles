<?php
namespace STALKER_CMS\Solutions\Articles\Models;

use Carbon\Carbon;
use STALKER_CMS\Vendor\Interfaces\ModelInterface;
use STALKER_CMS\Vendor\Models\BaseModel;
use STALKER_CMS\Vendor\Traits\ModelTrait;
use STALKER_CMS\Core\OpenGraph\Traits\OpenGraphTrait;
use STALKER_CMS\Core\Seo\Traits\SeoTrait;

class Rubric extends BaseModel implements ModelInterface {

    use ModelTrait, OpenGraphTrait, SeoTrait;
    protected $table = 'solution_articles_rubric';
    protected $fillable = ['slug', 'locale', 'template_id', 'title', 'announce', 'content', 'tags', 'order', 'user_id'];
    protected $hidden = [];
    protected $guarded = [];

    public function insert($request) {

        $this->slug = $request::input('slug');
        $this->locale = \App::getLocale();
        $this->template_id = $request::input('template_id');
        $this->title = $request::input('title');
        $this->announce = $request::input('announce');
        $this->content = $request::input('content');
        $this->main_image = $request::has('main_image') ? $request::input('main_image') : NULL;
        $this->announce_image = $request::has('announce_image') ? $request::input('announce_image') : NULL;
        $this->tags = $request::input('tags');
        $this->order = $this::max('order') + 1;
        $this->seo_title = $request::has('seo_title') && $request::input('seo_title') != '' ? $request::input('seo_title') : NULL;
        $this->seo_keywords = $request::has('seo_keywords') && $request::input('seo_keywords') != '' ? $request::input('seo_keywords') : NULL;
        $this->seo_description = $request::has('seo_description') && $request::input('seo_description') != '' ? $request::input('seo_description') : NULL;
        $this->seo_h1 = $request::has('seo_h1') && $request::input('seo_h1') != '' ? $request::input('seo_h1') : NULL;
        $this->seo_url = $request::has('seo_url') && $request::input('seo_url') != '' ? $request::input('seo_url') : NULL;
        $this->open_graph = $request::has('open_graph') ? $request::input('open_graph') : NULL;
        $this->user_id = \Auth::id();
        $this->created_at = Carbon::now();
        $this->updated_at = Carbon::now();
        $this->save();
        return $this;
    }

    public function replace($id, $request) {

        $model = $this::whereFind(['id' => $id]);
        $model->template_id = $request::input('template_id');
        $model->title = $request::input('title');
        $model->announce = $request::input('announce');
        $model->content = $request::input('content');
        $model->main_image = $request::has('main_image') ? $request::input('main_image') : NULL;
        $model->announce_image = $request::has('announce_image') ? $request::input('announce_image') : NULL;
        $model->tags = $request::input('tags');
        $model->seo_title = $request::has('seo_title') && $request::input('seo_title') != '' ? $request::input('seo_title') : NULL;
        $model->seo_keywords = $request::has('seo_keywords') && $request::input('seo_keywords') != '' ? $request::input('seo_keywords') : NULL;
        $model->seo_description = $request::has('seo_description') && $request::input('seo_description') != '' ? $request::input('seo_description') : NULL;
        $model->seo_h1 = $request::has('seo_h1') && $request::input('seo_h1') != '' ? $request::input('seo_h1') : NULL;
        $model->seo_url = $request::has('seo_url') && $request::input('seo_url') != '' ? $request::input('seo_url') : NULL;
        $model->open_graph = $request::has('open_graph') ? $request::input('open_graph') : NULL;
        $model->user_id = \Auth::id();
        $model->updated_at = Carbon::now();
        $model->save();
        return $model;
    }

    public function remove($id) {

        $instance = static::findOrFail($id);
        return $instance->delete();
    }

    public function search(array $attributes) {
        // TODO: Implement search() method.
    }

    public function filter(array $attributes) {
        // TODO: Implement filter() method.
    }

    public function sort(array $attributes) {
        // TODO: Implement sort() method.
    }

    public function getPublishedStartDateAttribute() {

        Carbon::setLocale(\App::getLocale());
        return Carbon::parse($this->attributes['published_start'])->diffForHumans();
    }

    public function getPublishedStopDateAttribute() {

        Carbon::setLocale(\App::getLocale());
        return Carbon::parse($this->attributes['published_stop'])->diffForHumans();
    }

    public function getPublishedDateAttribute() {

        if($this->attributes['published_at']):
            Carbon::setLocale(\App::getLocale());
            return Carbon::parse($this->attributes['published_at'])->diffForHumans();
        else:
            return '<span class="c-red">'.\Lang::get('core_content_lang::pages.not_published').'</span >';
        endif;
    }

    public function getClassColorAttribute() {

        if($this->attributes['publication']):
            return 'green';
        else:
            return 'bluegray';
        endif;
    }

    public function getAssetImageAttribute() {

        if($this->attributes['main_image']):
            return asset('uploads'.$this->attributes['main_image']);
        else:
            return NULL;
        endif;
    }

    public function getAssetAnnounceImageAttribute() {

        if($this->attributes['announce_image']):
            return asset('uploads'.$this->attributes['announce_image']);
        else:
            return NULL;
        endif;
    }

    /**
     * Шаблон страницы рубрики
     * @return \Illuminate\Database\Eloquent\Relations\hasOne
     */
    public function template() {

        return $this->hasOne('\STALKER_CMS\Solutions\Articles\Models\Template', 'id', 'template_id');
    }

    /**
     * Статьи рубрики
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function articles() {

        return $this->belongsTo('\STALKER_CMS\Solutions\Articles\Models\Article', 'rubric_id', 'id');
    }

    public function author() {

        return $this->hasOne('\STALKER_CMS\Core\System\Models\User', 'id', 'user_id');
    }

    /***************************************************************************************************************/
    public static function getStoreRules() {

        return ['slug' => 'required', 'template_id' => 'required', 'title' => 'required'];
    }

    public static function getUpdateRules() {

        return ['template_id' => 'required', 'title' => 'required'];
    }
}